import React, { useEffect, useState } from 'react'
import { useParams, useNavigate } from 'react-router-dom';
import AsistenDosenService from '../../services/AsistenDosen/AsistenDosenService'
import KoordinatorService from '../../services/AsistenDosen/KoordinatorService'

const ListAsistenDosenComponent = () => {
    const [principal, setPrincipal] = useState({})
    const [asistenDosens, setAsistenDosens] = useState([null])
    const [err, serErr] = useState(null)
    const { id, nama } = useParams();
    const [idMatkul, setIdMatkul] = useState(null)
    const history = useNavigate();

    useEffect(() => {
        setIdMatkul(id)
        AsistenDosenService.getAsistenDosenByIdMataKuliah(id).then((res) => {
            setAsistenDosens(res.data)
        }).catch((error) => {
            serErr(error)
        });
    }, [id]);

    useEffect(() => {
        const userData = localStorage.getItem("user")
        let user = ''
        if (typeof userData === 'object') {
            user = userData
        } else if (typeof userData === 'string') {
            user = JSON.parse(userData)
        }

        try {
            setPrincipal(user.principal)
        } catch (e) {
            setPrincipal(null)
        }
    }, []);

    function assignKoordinator(npm) {
        let assign = {
            npmMahasiswa: npm,
            idMataKuliah: id
        }
        KoordinatorService.assignKoordinator(assign);
        alert(`Asdos dengan ${npm} dijadikan Koordinator`)
        window.location.reload();
    }

    return (<>
        {(() => {
            if (err === null) {
                if (asistenDosens.length === 1 && asistenDosens[0] === null) {
                    return <div className="divLoader">
                        <svg className="svgLoader" viewBox="0 0 100 100" width="10em" height="10em">
                            <path ng-attr-d="{{config.pathCmd}}" ng-attr-fill="{{config.color}}" stroke="none" d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#51CACC" transform="rotate(179.719 50 51)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 51;360 50 51" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></path>
                        </svg>
                    </div>
                } else {
                    return <div className="flex max-w-screen-lg w-full mx-auto gap-6 items-start font-['Montserrat']">
                        <div className="flex-1 p-8 bg-white rounded-lg shadow">
                            <h1 className="font-bold text-3xl mb-9 underline decoration-[#780000] bg-[#EDEDED] text-[#0A0A0A] text-center">
                                Daftar AsistenDosen Mata Kuliah - {nama}
                            </h1>
                            <div className="flex flex-col gap-4">
                                <table className="table-auto">
                                    <thead className="text-l uppercase bg-[#EDEDED] text-[#0A0A0A]">
                                        <tr>
                                            <th>NPM Mahasiswa</th>
                                            <th>Nama Mahasiswa</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        {(() => {
                                            if (asistenDosens.length === 0) {
                                                return <tr className="bg-white border-b dark:bg-gray-50 dark:border-gray-20 
                                            hover:bg-gray-50 dark:hover:bg-[#E0E0E0] dark:text-[#424242] whitespace-nowrap">
                                                    <td colSpan="4">
                                                        <div className="flex flex-col flex-1 gap-2 text-center text-red-600">
                                                            Tidak ada Asisten Dosen pada Mata Kuliah ini
                                                        </div>
                                                    </td>
                                                </tr>
                                            } else {
                                                return <>
                                                    {
                                                        asistenDosens.map(
                                                            (asistenDosen, i) =>
                                                                <tr key={i} className="bg-white border-b dark:bg-gray-50 dark:border-gray-20 
                                                hover:bg-gray-50 dark:hover:bg-[#E0E0E0] dark:text-[#424242] whitespace-nowrap">
                                                                    <td>
                                                                        <div className="text-center">
                                                                            {asistenDosen.mahasiswa.npmMahasiswa}
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div className="text-center">
                                                                            {asistenDosen.mahasiswa.namaMahasiswa}
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div className="text-center">
                                                                            {(() => {
                                                                                if (asistenDosen.isKoordinator) {
                                                                                    return <>Koordinator</>
                                                                                } else {
                                                                                    return <>Asisten Dosen</>
                                                                                }
                                                                            })()}
                                                                        </div>
                                                                    </td>
                                                                    <td className="flex space-x-0.5 mt-1.5">
                                                                        {(() => {
                                                                            if (principal !== null) {
                                                                                if (principal.role === "DOSEN" || principal.role === "KOORDINATOR") {
                                                                                    return <>
                                                                                        {(() => {
                                                                                            if (!asistenDosen.isKoordinator) {
                                                                                                return <div className="flex flex-col flex-1 gap-2">
                                                                                                    <button onClick={() => assignKoordinator(asistenDosen.mahasiswa.npmMahasiswa)}
                                                                                                        className="focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">
                                                                                                        Assign Koordinator
                                                                                                    </button>
                                                                                                </div>
                                                                                            }
                                                                                        })()}
                                                                                        <div className="flex flex-col flex-1 gap-2">
                                                                                        <button onClick={() => history('/my-mata-kuliah/daftar-asisten/detail-asisten/' + idMatkul + '/' + asistenDosen.mahasiswa.akun.email)}
                                                                                                className="focus:outline-none text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                                                                                Detail
                                                                                            </button>
                                                                                        </div>
                                                                                    </>
                                                                                } else if (principal.role === "ADMIN") {
                                                                                    return <>
                                                                                        <div className="flex flex-col flex-1 gap-2">
                                                                                            <button onClick={() => history('/my-mata-kuliah/daftar-asisten/detail-asisten/' + idMatkul + '/' + asistenDosen.mahasiswa.akun.email)}
                                                                                                className="focus:outline-none text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                                                                                Detail
                                                                                            </button>
                                                                                        </div>
                                                                                        <div className="flex flex-col flex-1 gap-2">
                                                                                            <button onClick={() => history('/my-mata-kuliah/daftar-asisten/log-asisten/' + idMatkul + '/' + asistenDosen.mahasiswa.npmMahasiswa)}
                                                                                                className="focus:outline-none text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                                                                                Log Asisten
                                                                                            </button>
                                                                                        </div>
                                                                                    </>
                                                                                }
                                                                            }
                                                                        })()}
                                                                    </td>
                                                                </tr>
                                                        )
                                                    }
                                                </>
                                            }
                                        })()}

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                }
            } else {
                return <div role="alert">
                    <div className="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                        ERR_CONNECTION_REFUSED
                    </div>
                </div>
            }
        })()}
    </>

    )
}

export default ListAsistenDosenComponent