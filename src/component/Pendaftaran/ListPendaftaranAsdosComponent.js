import React, { useEffect, useState } from 'react'
import { useParams, useNavigate } from 'react-router-dom';
import PendaftaranService from '../../services/AsistenDosen/PendaftaranService'

const ListPendaftaranAsdosComponent = () => {
    const [principal, setPrincipal] = useState({})
    const [pendaftarans, setPendaftarans] = useState([null])
    const [err, serErr] = useState(null)
    const [idMatkul, setIdMatkul] = useState(null)
    const { id, nama } = useParams();
    const history = useNavigate();

    useEffect(() => {
        setIdMatkul(id)
        PendaftaranService.getPendaftaranByIdMataKuliah(id).then((res) => {
            console.log(res.data)
            setPendaftarans(res.data)
        }).catch((error) => {
            serErr(error)
        });
    }, [id]);

    useEffect(() => {
        const userData = localStorage.getItem("user")
        let user = ''
        if (typeof userData === 'object') {
            user = userData
        } else if (typeof userData === 'string') {
            user = JSON.parse(userData)
        }

        try {
            setPrincipal(user.principal)
        } catch (e) {
            setPrincipal(null)
        }
    }, []);

    function actionPendaftaran(npm, stringAction) {
        let action = {
            npmMahasiswa: npm,
            action: stringAction
        }
        PendaftaranService.actionPendaftaran(id, action);
        alert(`Mahasiswa dengan ${npm} : ${stringAction}`)
        window.location.reload();
    }

    return (<>
        {(() => {
            if (err === null) {
                if (pendaftarans.length === 1 && pendaftarans[0] === null) {
                    return <div className="divLoader">
                        <svg className="svgLoader" viewBox="0 0 100 100" width="10em" height="10em">
                            <path ng-attr-d="{{config.pathCmd}}" ng-attr-fill="{{config.color}}" stroke="none" d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#51CACC" transform="rotate(179.719 50 51)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 51;360 50 51" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></path>
                        </svg>
                    </div>
                } else {
                    return <div className="flex max-w-screen-lg w-full mx-auto gap-6 items-start font-['Montserrat']">
                        <div className="flex-1 p-8 bg-white rounded-lg shadow">
                            <h1 className="font-bold text-3xl mb-9 underline decoration-[#780000] bg-[#EDEDED] text-[#0A0A0A] text-center">
                                Daftar Pendaftaran Mata Kuliah - {nama}
                            </h1>
                            <div className="flex flex-col gap-4">
                                <table className="table-auto">
                                    <thead className="text-l uppercase bg-[#EDEDED] text-[#0A0A0A]">
                                        <tr>
                                            <th>Nama Mahasiswa</th>
                                            <th>SKS Mahasiswa</th>
                                            <th>IPK Mahasiswa</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        {(() => {
                                            if (pendaftarans.length === 0) {
                                                return <tr className="bg-white border-b dark:bg-gray-50 dark:border-gray-20 
                                            hover:bg-gray-50 dark:hover:bg-[#E0E0E0] dark:text-[#424242] whitespace-nowrap">
                                                    <td colSpan="4">
                                                        <div className="flex flex-col flex-1 gap-2 text-center text-red-600">
                                                            Tidak ada pendaftaran Asisten Dosen pada Mata Kuliah ini
                                                        </div>
                                                    </td>
                                                </tr>
                                            } else {
                                                return <>
                                                    {
                                                        pendaftarans.map((pendaftaran, id) =>
                                                            <tr className="bg-white border-b dark:bg-gray-50 dark:border-gray-20 
                                                hover:bg-gray-50 dark:hover:bg-[#E0E0E0] dark:text-[#424242] whitespace-nowrap">
                                                                <td>
                                                                    <div className="text-center">
                                                                        {pendaftaran.mahasiswa.namaMahasiswa}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="text-center">
                                                                        {pendaftaran.sks}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="text-center">
                                                                        {pendaftaran.ipk}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="text-center">
                                                                        {pendaftaran.statusPendaftaran}
                                                                    </div>
                                                                </td>
                                                                <td className="flex space-x-0.5 mt-1.5">
                                                                    {(() => {
                                                                        if (principal !== null) {
                                                                            return <>
                                                                                {(() => {
                                                                                    if (principal.role === "ADMIN") {
                                                                                        if (pendaftaran.statusPendaftaran === 'MENDAFTAR' || pendaftaran.statusPendaftaran === 'REKOMENDASI') {
                                                                                            return <>
                                                                                                <div className="flex flex-col flex-1 gap-2">
                                                                                                    <button onClick={() => actionPendaftaran(pendaftaran.mahasiswa.npmMahasiswa, 'DITERIMA')}
                                                                                                        className="focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">
                                                                                                        Terima
                                                                                                    </button>
                                                                                                </div>
                                                                                                <div className="flex flex-col flex-1 gap-2">
                                                                                                    <button onClick={() => actionPendaftaran(pendaftaran.mahasiswa.npmMahasiswa, 'DITOLAK')}
                                                                                                        className="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800">
                                                                                                        Tolak
                                                                                                    </button>
                                                                                                </div>
                                                                                            </>
                                                                                        }
                                                                                    } else if (principal.role === "DOSEN") {
                                                                                        if (pendaftaran.statusPendaftaran === 'MENDAFTAR') {
                                                                                            return <div className="flex flex-col flex-1 gap-2">
                                                                                                <button onClick={() => actionPendaftaran(pendaftaran.mahasiswa.npmMahasiswa, 'REKOMENDASI', id)}
                                                                                                    className="focus:outline-none text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                                                                                    Rekomendasi
                                                                                                </button>
                                                                                            </div>
                                                                                        }
                                                                                    }
                                                                                })()}
                                                                                <div className="flex flex-col flex-1 gap-2">
                                                                                    <button onClick={() => history('/asisten-dosen/detail-pendaftar/' + idMatkul + '/' + pendaftaran.mahasiswa.akun.email)}
                                                                                        className="focus:outline-none text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                                                                        Detail
                                                                                    </button>
                                                                                </div>
                                                                            </>
                                                                        }
                                                                    })()}
                                                                </td>
                                                            </tr>
                                                        )
                                                    }
                                                </>
                                            }
                                        })()}

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                }
            } else {
                return <div role="alert">
                    <div className="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                        ERR_CONNECTION_REFUSED
                    </div>
                </div>
            }
        })()}
    </>

    )
}

export default ListPendaftaranAsdosComponent