import React, { useEffect, useState } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import MahasiswaService from '../../services/Pengguna/MahasiswaService'
import PendaftaranService from '../../services/AsistenDosen/PendaftaranService'

const CreateMataKuliahComponent = () => {
    const [sks, setSks] = useState('')
    const [ipk, setIpk] = useState('')
    const { id, nama } = useParams();
    const [mahasiswa, setMahasiswa] = useState('')
    const history = useNavigate();

    const save = (e) => {
        e.preventDefault();
        let pendaftaran = {
            idMataKuliah: id,
            npmMahasiswa: mahasiswa.npmMahasiswa,
            sks: sks,
            ipk: ipk
        }

        async function fetchMyAPI() {
            try {
                await PendaftaranService.createPendaftaran(pendaftaran);
                history('/mata-kuliah');
            } catch(e) {
                alert("Anda telah daftar pada mata kuliah ini. Silahkan coba pada mata kuliah lainnya")
            }
        }

        fetchMyAPI();
    }

    useEffect(() => {
        async function fetchMyAPI() {
            const userData = localStorage.getItem("user")
            let user = ''
            if (typeof userData === 'object') {
                user = userData
            } else if (typeof userData === 'string') {
                user = JSON.parse(userData)
            }
            if (user !== '') {
                MahasiswaService.getMahasiswaByEmail(user.name).then((res) => {
                    setMahasiswa(res.data)
                })
            }
        }

        fetchMyAPI();
    }, []);

    const inputStyle = "appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"

    return (
        <div className="flex max-w-screen-lg w-full mx-auto gap-6 items-start font-['Montserrat']">
            <div className="flex-1 p-8 bg-white rounded-lg shadow">
                <h1 className="font-bold text-3xl mb-9 underline decoration-[#40606C] text-center">Formulir Registrasi Asisten Dosen</h1>
                <form
                    className="mt-4 flex flex-col flex-1 gap-2 form-matkul"
                    method="POST"
                    autoComplete="off">
                    <label className="text-gray-700 text-sm font-bold" htmlFor="mataKuliah"> Nama Mata Kuliah </label>
                    <p>{nama}</p>
                    <label className="text-gray-700 text-sm font-bold" htmlFor="mahasiswa"> Nama Mahasiswa </label>
                    <p>{mahasiswa.namaMahasiswa}</p>
                    <label className="text-gray-700 text-sm font-bold" htmlFor="ipk"> IPK </label>
                    <input
                        type="text"
                        name="ipk"
                        value={ipk}
                        placeholder="Masukkan IPK Anda"
                        className={inputStyle}
                        onChange={(e) => setIpk(e.target.value)}
                    />
                    <label className="text-gray-700 text-sm font-bold" htmlFor="sks"> SKS </label>
                    <input
                        type="text"
                        name="ipk"
                        value={sks}
                        placeholder="Masukkan SKS Anda"
                        className={inputStyle}
                        onChange={(e) => setSks(e.target.value)}
                    />
                    <button
                        className="transition ease-in-out delay-150 mx-72 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800"
                        type="submit"
                        onClick={(e) => save(e)}>
                        Daftar
                    </button>
                </form>
            </div>
        </div>
    )
}

export default CreateMataKuliahComponent