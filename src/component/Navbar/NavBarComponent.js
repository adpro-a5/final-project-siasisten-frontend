import logo from '../../static/logo.png';
import React, { useEffect, useState } from 'react'

const NavBarComponent = () => {
    const [principal, setPrincipal] = useState({})

    useEffect(() => {
        const userData = localStorage.getItem("user")
        let user = ''
        if (typeof userData === 'object') {
            user = userData
        } else if (typeof userData === 'string') {
            user = JSON.parse(userData)
        }

        try {
            setPrincipal(user.principal)
        } catch (e) {
            setPrincipal(null)
        }
    }, []);

    return (
        <nav fragment="navbar"
            className="sticky top-0 z-50 flex flex-none bg-white border-gray-200 px-2 sm:px-4 py-3.5 rounded dark:bg-[#D0D1C7] font-['Montserrat']">
            <div className="container flex flex-wrap justify-between items-center mx-auto">
                <a href="/mata-kuliah" className="flex items-center">
                    <img style={{ height: "24px", width: "107px" }} src={logo} alt="siasisten+" />
                </a>
                <div className="hidden w-full md:block md:w-auto" id="mobile-menu">
                    <ul className="flex flex-col mt-4 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium">
                        {(() => {
                            if (principal !== null) {
                                return <>
                                    {(() => {
                                        if (principal.role === 'ADMIN') {
                                            return <>
                                                <li>
                                                    <a href="/dosen" className="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-[#003049] md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                                                        Dosen
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/mahasiswa" className="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-[#003049] md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                                                        Mahasiswa
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/my-mata-kuliah" className="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-[#003049] md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                                                        My Mata Kuliah
                                                    </a>
                                                </li>
                                            </>
                                        } else if (principal.role === 'DOSEN') {
                                            return <>
                                                <li>
                                                    <a href="/my-mata-kuliah" className="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-[#003049] md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                                                        My Mata Kuliah
                                                    </a>
                                                </li>
                                            </>
                                        } else if (principal.role === 'KOORDINATOR') {
                                            return <>
                                                <li>
                                                    <a href="/my-mata-kuliah" className="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-[#003049] md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                                                        My Mata Kuliah
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/log-asisten" className="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-[#003049] md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                                                        Log Asisten
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/laporan-log-asisten" className="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-[#003049] md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                                                        Laporan Log
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/profile-mahasiswa" className="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-[#003049] md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                                                        Profile
                                                    </a>
                                                </li>
                                            </>
                                        } else if (principal.role === 'ASISTENDOSEN') {
                                            return <>
                                                <li>
                                                    <a href="/my-mata-kuliah" className="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-[#003049] md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                                                        My Mata Kuliah
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/log-asisten" className="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-[#003049] md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                                                        Log Asisten
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/laporan-log-asisten" className="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-[#003049] md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                                                        Laporan Log
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/profile-mahasiswa" className="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-[#003049] md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                                                        Profile
                                                    </a>
                                                </li>
                                            </>
                                        } else if (principal.role === 'MAHASISWA') {
                                            return <>
                                                <li>
                                                    <a href="/profile-mahasiswa" className="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-[#003049] md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                                                        Profile
                                                    </a>
                                                </li>
                                            </>
                                        }
                                    })()}
                                    <li>
                                        <a href="/logout" className="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-[#003049] md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                                            Sign Out
                                        </a>
                                    </li>
                                </>
                            } else {
                                return <li>
                                    <a href="/" className="block py-2 pr-4 pl-3 text-gray-700 border-b border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-[#003049] md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                                        Login
                                    </a>
                                </li>
                            }
                        })()}
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default NavBarComponent