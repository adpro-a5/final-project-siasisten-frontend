import React from 'react';
import NavBar from "./NavBarComponent"
import { Outlet } from 'react-router';

const WithNav = () => {
  return (
    <>
      <NavBar />
      <Outlet />
    </>
  );
};

export default WithNav