import React, { useState } from 'react'
import KriteriaPenilaianService from '../../services/Penilaian/KriteriaPenilaianService'
import { useNavigate, useParams } from 'react-router-dom';

const FormKriteriaPenilaianComponent = () => {
    const history = useNavigate();
    const { id } = useParams('')
    const [kriteria, setKriteria] = useState([])

    const onChangeKriteria = (namaNilai, idx) => {
        const updateKriteria = [...kriteria];
        updateKriteria[idx] = namaNilai;
        setKriteria(updateKriteria);
    }

    const save = (e) => {
        e.preventDefault();
        let kriteriaPenilaian = {
            idMataKuliah: id,
            listKriteriaPenilaian: kriteria
        }
        KriteriaPenilaianService.createKriteria(kriteriaPenilaian)
        alert('Kriteria akan ditambahkan')
        history("/my-mata-kuliah/penilaian-mahasiswa/" + id)
    }

    const inputStyle = "appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"

    return (
        <div className="bg-white rounded-lg shadow p-6 flex flex-col">
            <h3 className="text-xl font-bold">Kriteria Penilaian Form</h3>
            <form
                className="mt-4 flex flex-col flex-1 gap-2 form-kriteria-penilaian"
                method="POST"
                autoComplete="off">
                <p className="text-gray-700 text-sm font-bold"> ID Mata Kuliah: </p>
                <div>
                    {id}
                </div>
                <div>
                    <label className="text-gray-700 text-sm font-bold" htmlFor="jumlahKriteria"> Jumlah Kriteria </label>
                    <input
                        id="jumlah-kriteria"
                        type="number"
                        name="jumlahKriteria"
                        placeholder="Masukkan Jumlah Kriteria"
                        className={inputStyle}
                        value={kriteria.length}
                        onChange={(e) => setKriteria(new Array(parseInt(e.target.value)).fill(''))}
                    />
                </div>
                {kriteria.map((namaNilai, i) => (
                    <div key={i}>
                        <label className="text-gray-700 text-sm font-bold" htmlFor={`kriteria${i}`}> Kriteria ke-{i + 1} </label>
                        <input
                            type="text"
                            name={`kriteria${i}`}
                            placeholder={`Masukkan Kriteria Ke-${i + 1}`}
                            className={inputStyle}
                            onChange={(e) => onChangeKriteria(e.target.value, i)}
                        />
                    </div>
                ))}
                <p className="text-xs">Contoh: Lab 1 / Tugas Mandiri 1</p>
                <button
                    className="transition ease-in-out delay-150 mx-72 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800"
                    type="submit"
                    onClick={(e) => save(e)}>
                    Daftar
                </button>
            </form>
        </div>
    )
}

export default FormKriteriaPenilaianComponent