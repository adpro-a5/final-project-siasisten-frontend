import React, { useEffect, useState } from 'react'
import KriteriaPenilaianService from '../../services/Penilaian/KriteriaPenilaianService'
import MahasiswaMatkulService from '../../services/Penilaian/MahasiswaMatkulService'
import PenilaianMahasiswaService from '../../services/Penilaian/PenilaianMahasiswaService'
import { useNavigate, useParams } from 'react-router-dom';

const PenilaianMahasiswaEditComponent = () => {
    const history = useNavigate();
    const { id } = useParams('')
    const [allKriteria, setAllKriteria] = useState([null])
    const [allMahasiswa, setAllMahasiswa] = useState([null])
    const [allPenilaian, setAllPenilaian] = useState([null])

    useEffect(() => {
        async function fetchMyAPI() {
            KriteriaPenilaianService.getKriteriaByIdMataKuliah(id).then((res) => {
                setAllKriteria(res.data)
            });

            MahasiswaMatkulService.getMahasiswaByIdMataKuliah(id).then((res) => {
                setAllMahasiswa(res.data)
            });

            PenilaianMahasiswaService.getPenilaianByIdMataKuliah(id).then((res) => {
                setAllPenilaian(res.data)
            });
        }

        fetchMyAPI();
    }, [id]);

    const onChangeNilai = (nilai, idx) => {
        const updateAllPenilaian = [...allPenilaian];
        updateAllPenilaian[idx].nilai = nilai;
        setAllPenilaian(updateAllPenilaian);
    }   

    const save = (e) => {
        e.preventDefault();
        let penilaian = {
            penilaianMahasiswaList: allPenilaian
        }
        PenilaianMahasiswaService.updateNilai(penilaian);
        history('/my-mata-kuliah/penilaian-mahasiswa/' + id)
    }

    return (
        <>
            {(() => {
                if ((allKriteria.length === 1 && allKriteria[0] === null) ||
                    (allMahasiswa.length === 1 && allMahasiswa[0] === null) ||
                    (allPenilaian.length === 1 && allPenilaian[0] === null)) {
                    return <div className="divLoader">
                        <svg className="svgLoader" viewBox="0 0 100 100" width="10em" height="10em">
                            <path ng-attr-d="{{config.pathCmd}}" ng-attr-fill="{{config.color}}" stroke="none" d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#51CACC" transform="rotate(179.719 50 51)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 51;360 50 51" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></path>
                        </svg>
                    </div>
                } else {
                    return <>
                        <div className="flex max-w-screen-lg w-full mx-auto gap-6 items-start font-['Montserrat']">
                            <div className="flex-1 my-8 p-8 bg-white rounded-lg shadow">
                                <h1 className="font-bold text-3xl mb-4 decoration-[#780000] text-center">Penilaian</h1>
                                <div className="flex flex-col gap-4">
                                    <form
                                        className="mt-4 flex flex-col flex-1"
                                        method="POST"
                                        autoComplete="off">
                                        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tblData">
                                            <thead className="text-gray-700 uppercase bg-gray-50 dark:bg-gray-100 dark:text-gray-800">
                                                <tr>
                                                    <th scope="col" className="px-6 py-3">
                                                        NPM
                                                    </th>
                                                    {
                                                        allKriteria.map(
                                                            kriteria => <th key={kriteria.id} scope="col" className="px-6 py-3">
                                                                {kriteria.namaNilai}
                                                            </th>
                                                        )
                                                    }
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    allMahasiswa.map((mahasiswa, i) => (
                                                        <tr key={i} className="border-b dark:bg-gray-800 dark:border-gray-700 odd:bg-white even:bg-gray-50 odd:dark:bg-gray-800 even:dark:bg-gray-700">
                                                            <td className="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                                                                {mahasiswa.npmMahasiswa}
                                                            </td>
                                                            {allKriteria.map((kriteria, j) => (<>
                                                                {(() => {
                                                                    if (allPenilaian.length !== 0) {
                                                                        return <>
                                                                            <td key={j + (i * allKriteria.length)} className="px-6 py-4">
                                                                                <input
                                                                                    className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                                                                                    id="inline-full-name"
                                                                                    type="text"
                                                                                    name={`nilai + ${j + (i * allKriteria.length)}`}
                                                                                    value={allPenilaian[j + (i * allKriteria.length)].nilai}
                                                                                    size="4"
                                                                                    onChange={(e) => onChangeNilai(e.target.value, j + (i * allKriteria.length))} />
                                                                            </td>
                                                                        </>
                                                                    }
                                                                })()}
                                                            </>))}
                                                        </tr>
                                                    ))
                                                }
                                            </tbody>
                                        </table>
                                        <div className="flex flex-1 justify-center">
                                            <button onClick={(e) => save(e)} className="w-1/6 px-6 py-2 mt-4 text-white bg-sky-600 rounded-lg hover:bg-sky-900" type="submit">
                                                Simpan
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </>
                }
            })()}
        </>
    )
}

export default PenilaianMahasiswaEditComponent