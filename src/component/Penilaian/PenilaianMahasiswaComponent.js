import React, { useEffect, useState } from 'react'
import KriteriaPenilaianService from '../../services/Penilaian/KriteriaPenilaianService'
import MahasiswaMatkulService from '../../services/Penilaian/MahasiswaMatkulService'
import PenilaianMahasiswaService from '../../services/Penilaian/PenilaianMahasiswaService'
import { useParams } from 'react-router-dom';

const PenilaianMahasiswaComponent = () => {
    const { id } = useParams('')
    const [allKriteria, setAllKriteria] = useState([null])
    const [allMahasiswa, setAllMahasiswa] = useState([null])
    const [allPenilaian, setAllPenilaian] = useState([null])

    useEffect(() => {
        async function fetchMyAPI() {
            KriteriaPenilaianService.getKriteriaByIdMataKuliah(id).then((res) => {
                setAllKriteria(res.data)
            });

            MahasiswaMatkulService.getMahasiswaByIdMataKuliah(id).then((res) => {
                setAllMahasiswa(res.data)
            });

            PenilaianMahasiswaService.getPenilaianByIdMataKuliah(id).then((res) => {
                setAllPenilaian(res.data)
            });
        }

        fetchMyAPI();
    }, [id]);

    const download = () => {
        var downloadLink;
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementById("tblData");
        var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

        // Specify file name
        var filename = `penilaian-${id}.xls`;

        // Create download link element
        downloadLink = document.createElement("a");

        document.body.appendChild(downloadLink);

        if (navigator.msSaveOrOpenBlob) {
            var blob = new Blob(['\ufeff', tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob(blob, filename);
        } else {
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

            // Setting the file name
            downloadLink.download = filename;

            //triggering the function
            downloadLink.click();
        }
    }

    return (
        <>
            {(() => {
                if ((allKriteria.length === 1 && allKriteria[0] === null) ||
                    (allMahasiswa.length === 1 && allMahasiswa[0] === null) ||
                    (allPenilaian.length === 1 && allPenilaian[0] === null)) {
                    return <div className="divLoader">
                        <svg className="svgLoader" viewBox="0 0 100 100" width="10em" height="10em">
                            <path ng-attr-d="{{config.pathCmd}}" ng-attr-fill="{{config.color}}" stroke="none" d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#51CACC" transform="rotate(179.719 50 51)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 51;360 50 51" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></path>
                        </svg>
                    </div>
                } else {
                    return <>
                        <div className="flex max-w-screen-lg w-full mx-auto gap-6 items-start font-['Montserrat']">
                            <div className="flex-1 my-8 p-8 bg-white rounded-lg shadow">
                                <h1 className="font-bold text-3xl mb-9 decoration-[#780000] text-center">Penilaian</h1>
                                <div className="flex flex-col gap-4">
                                    <form
                                        className="mt-4 flex flex-col flex-1"
                                        method="POST"
                                        autoComplete="off">
                                        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tblData">
                                            <thead className="text-gray-700 uppercase bg-gray-50 dark:bg-gray-100 dark:text-gray-800">
                                                <tr>
                                                    <th scope="col" className="px-6 py-3">
                                                        NPM
                                                    </th>
                                                    {
                                                        allKriteria.map(
                                                            kriteria => <th key={kriteria.id} scope="col" className="px-6 py-3">
                                                                {kriteria.namaNilai}
                                                            </th>
                                                        )
                                                    }
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    allMahasiswa.map((mahasiswa, i) => (
                                                        <tr key={i} className="border-b dark:bg-gray-800 dark:border-gray-700 odd:bg-white even:bg-gray-50 odd:dark:bg-gray-800 even:dark:bg-gray-700">
                                                            <td className="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                                                                {mahasiswa.npmMahasiswa}
                                                            </td>
                                                            {allKriteria.map((kriteria, j) => (<>
                                                                {(() => {
                                                                    if (allPenilaian.length !== 0) {
                                                                        return <>
                                                                            <td key={j + (i * allKriteria.length)} className="px-6 py-4">
                                                                                {allPenilaian[j + (i * allKriteria.length)].nilai}
                                                                            </td>
                                                                        </>
                                                                    }
                                                                })()}
                                                            </>))}
                                                        </tr>
                                                    ))
                                                }
                                            </tbody>
                                        </table>
                                        <div className="flex flex-1 justify-center">
                                            <a href={'edit/' + id} className="edit px-6 py-2 mr-2 mt-4 text-white bg-sky-600 rounded-lg hover:bg-sky-900">
                                                Edit
                                            </a>
                                        </div>
                                    </form>
                                </div>
                                <button onClick={download}
                                    className="mt-5 bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">
                                    <svg className="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M13 8V2H7v6H2l8 8 8-8h-5zM0 18h20v2H0v-2z" /></svg>
                                    <span>Download</span>
                                </button>
                            </div>
                        </div>
                        <div className="flex flex-1 justify-center">
                            <a className="px-6 py-2 mr-2 mt-4 text-white bg-sky-600 rounded-lg hover:bg-sky-900"
                                href={'add-kriteria/' + id}>
                                Tambah Kriteria
                            </a>
                            <a className="px-6 py-2 mt-4 text-white bg-sky-600 rounded-lg hover:bg-sky-900"
                                href={'add-mahasiswa/' + id}>
                                Tambah Mahasiswa
                            </a>
                        </div>
                    </>
                }
            })()}
        </>
    )
}

export default PenilaianMahasiswaComponent