import React, { useState } from 'react'
import MahasiswaMatkulService from '../../services/Penilaian/MahasiswaMatkulService'
import { useNavigate, useParams } from 'react-router-dom';

const FormMahasiswaMatkulComponent = () => {
    const history = useNavigate();
    const { id } = useParams('')
    const [mahasiswa, setMahasiswa] = useState([])

    const onChangeMahasiswa = (npm, idx) => {
        const updateMahasiswa = [...mahasiswa];
        updateMahasiswa[idx] = npm;
        setMahasiswa(updateMahasiswa);
    }

    const save = (e) => {
        e.preventDefault();
        let mahasiswaMatkul = {
            idMataKuliah: id,
            listMahasiswaMatkul: mahasiswa
        }
        MahasiswaMatkulService.createMahasiswaMatkul(mahasiswaMatkul)
        alert('Mahasiswa akan ditambahkan')
        history("/my-mata-kuliah/penilaian-mahasiswa/" + id)
    }

    const inputStyle = "appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"

    return (
        <div className="bg-white rounded-lg shadow p-6 flex flex-col">
            <h3 className="text-xl font-bold">Mahasiswa - Mata Kuliah Form</h3>
            <form
                className="mt-4 flex flex-col flex-1 gap-2 form-kriteria-penilaian"
                method="POST"
                autoComplete="off">
                <p className="text-gray-700 text-sm font-bold"> ID Mata Kuliah: </p>
                <div>
                    ID
                </div>
                <div>
                    <label className="text-gray-700 text-sm font-bold" htmlFor="jumlahMahasiswa"> Jumlah Mahasiswa </label>
                    <input
                        id="jumlah-mahasiswa"
                        type="number"
                        name="jumlahMahasiswa"
                        placeholder="Masukkan Jumlah Mahasiswa"
                        className={inputStyle}
                        value={mahasiswa.length}
                        onChange={(e) => setMahasiswa(new Array(parseInt(e.target.value)).fill(''))}
                    />
                </div>
                {mahasiswa.map((npm, i) => (
                    <div key={i}>
                        <label className="text-gray-700 text-sm font-bold" htmlFor={`mahasiswa${i}`}> Mahasiswa ke-{i + 1} </label>
                        <input
                            type="text"
                            name={`mahasiswa${i}`}
                            placeholder={`Masukkan Mahasiswa Ke-${i + 1}`}
                            className={inputStyle}
                            onChange={(e) => onChangeMahasiswa(e.target.value, i)}
                        />
                    </div>
                ))}
                <button
                    className="transition ease-in-out delay-150 mx-72 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800"
                    type="submit"
                    onClick={(e) => save(e)}>
                    Daftar
                </button>
            </form>
        </div>
    )
}

export default FormMahasiswaMatkulComponent