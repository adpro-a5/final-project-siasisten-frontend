import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import LogAsistenService from '../../services/LogAsisten/LogAsistenService'

const UpdateLogAsistenComponent = () => {
    const { id } = useParams('')

    const [principal, setPrincipal] = useState({})
    const [asdos, setAsdos] = useState(null)
    const [log, setLog] = useState([]);

    const [idMataKuliah, setIdMataKuliah] = useState(null);
    const [selectedDate, setSelectedDate] = useState(null);
    const [selectedMonth, setSelectedMonth] = useState(null);
    const [selectedYear, setSelectedYear] = useState(null);
    const [waktuMulai, setWaktuMulai] = useState([])
    const [waktuSelesai, setWaktuSelesai] = useState([])
    const [selectedKategori, setSelectedKategori] = useState(null);
    const [deskripsi, setDeskripsi] = useState('')

    const [wrongSubmit, setWrongSubmit] = useState(false)
    const [wrongTime, setWrongTime] = useState(false)

    const date = [
        { value: 1, label: '01' },
        { value: 2, label: '02' },
        { value: 3, label: '03' },
        { value: 4, label: '04' },
        { value: 5, label: '05' },
        { value: 6, label: '06' },
        { value: 7, label: '07' },
        { value: 8, label: '08' },
        { value: 9, label: '09' },
        { value: 10, label: '10' },
        { value: 11, label: '11' },
        { value: 12, label: '12' },
        { value: 14, label: '13' },
        { value: 14, label: '14' },
        { value: 15, label: '15' },
        { value: 16, label: '16' },
        { value: 17, label: '17' },
        { value: 18, label: '18' },
        { value: 19, label: '19' },
        { value: 20, label: '20' },
        { value: 21, label: '21' },
        { value: 22, label: '22' },
        { value: 23, label: '23' },
        { value: 24, label: '24' },
        { value: 25, label: '25' },
        { value: 26, label: '26' },
        { value: 27, label: '27' },
        { value: 28, label: '28' },
        { value: 29, label: '29' },
        { value: 30, label: '30' },
        { value: 31, label: '31' },
    ];
    const month = [
        { value: 'Januari', label: 'Januari' },
        { value: 'Februari', label: 'Februari' },
        { value: 'Maret', label: 'Maret' },
        { value: 'April', label: 'April' },
        { value: 'Mei', label: 'Mei' },
        { value: 'Juni', label: 'Juni' },
        { value: 'Juli', label: 'Juli' },
        { value: 'Agustus', label: 'Agustus' },
        { value: 'September', label: 'September' },
        { value: 'Oktober', label: 'Oktober' },
        { value: 'November', label: 'November' },
        { value: 'Desember', label: 'Desember' },
    ];
    const year = [
        { value: 2020, label: '2020' },
        { value: 2021, label: '2021' },
        { value: 2022, label: '2022' },
    ];
    const kategori = [
        { value: 'Asistensi/Tutorial', label: 'Asistensi/Tutorial' },
        { value: 'Mengoreksi', label: 'Mengoreksi' },
        { value: 'Mengawas', label: 'Mengawas' },
        { value: 'Persiapan Asistensi', label: 'Persiapan Asistensi' },
        { value: 'Membuat soal/tugas', label: 'Membuat soal/tugas' },
        { value: 'Rapat', label: 'Rapat' },
        { value: 'Sit in Kelas', label: 'Sit in Kelas' },
        { value: 'Pengembangan Materi', label: 'Pengembangan Materi' },
        { value: 'Pengembangan apps', label: 'Pengembangan apps' },
        { value: 'Persiapan infra', label: 'Persiapan infra' },
        { value: 'Dokumentasi', label: 'Dokumentasi' },
        { value: 'Persiapan kuliah', label: 'Persiapan kuliah' },
        { value: 'Penunjang', label: 'Penunjang' },
        { value: 'Input Data', label: 'Input Data' },
    ];

    const history = useNavigate();

    useEffect(() => {
        async function fetchMyAPI() {
            await LogAsistenService.getLogAsistenByIdLog(id).then((res) => {
                setLog(res.data)
            });
        }

        fetchMyAPI();
    }, [id]);

    useEffect(() => {
        if (log.length !== 0) {
            setIdMataKuliah(log.idMataKuliah)
            setSelectedDate(log.tanggalLogAsisten)
            setSelectedMonth(log.bulanLogAsisten)
            setSelectedYear(log.tahunLogAsisten)

            let mulai = []
            if (log.jamWaktuMulaiLogAsisten < 10) {
                mulai.push('0' + log.jamWaktuMulaiLogAsisten)
            } else {
                mulai.push(log.jamWaktuMulaiLogAsisten)
            }

            if (log.menitWaktuMulaiLogAsisten < 10) {
                mulai.push('0' + log.menitWaktuMulaiLogAsisten)
            } else {
                mulai.push(log.menitWaktuMulaiLogAsisten)
            }

            if (log.detikWaktuMulaiLogAsisten < 10) {
                mulai.push('0' + log.detikWaktuMulaiLogAsisten)
            } else {
                mulai.push(log.detikWaktuMulaiLogAsisten)
            }
            setWaktuMulai(mulai.join(":"))

            let selesai = []
            if (log.jamWaktuSelesaiLogAsisten < 10) {
                selesai.push('0' + log.jamWaktuSelesaiLogAsisten)
            } else {
                selesai.push(log.jamWaktuSelesaiLogAsisten)
            }

            if (log.menitWaktuSelesaiLogAsisten < 10) {
                selesai.push('0' + log.menitWaktuSelesaiLogAsisten)
            } else {
                selesai.push(log.menitWaktuSelesaiLogAsisten)
            }

            if (log.detikWaktuSelesaiLogAsisten < 10) {
                selesai.push('0' + log.detikWaktuSelesaiLogAsisten)
            } else {
                selesai.push(log.detikWaktuSelesaiLogAsisten)
            }
            setWaktuSelesai(selesai.join(":"))

            setSelectedKategori(log.kategori)
            setDeskripsi(log.deskripsiTugas)
        }
    }, [log]);

    useEffect(() => {
        const userData = localStorage.getItem("user")
        let user = ''
        if (typeof userData === 'object') {
            user = userData
        } else if (typeof userData === 'string') {
            user = JSON.parse(userData)
        }

        try {
            setPrincipal(user.principal)
        } catch (e) {
            setPrincipal(null)
        }
    }, []);

    useEffect(() => {
        if (principal !== null && (principal.role === "ASISTENDOSEN" || principal.role === "KOORDINATOR")) { // Ubah
            const asdosData = localStorage.getItem("asdos")
            let asdos = ''
            if (typeof asdosData === 'object') {
                asdos = asdosData
            } else if (typeof asdosData === 'string') {
                asdos = JSON.parse(asdosData)
            }

            try {
                setAsdos(asdos)
            } catch (e) {
                setAsdos(null)
            }
        }
    }, [principal]);

    function handleNavigate() {
        history('/mata-kuliah');
    }

    function changeWaktuMulai(e) {
        e.preventDefault();
        setWaktuMulai(e.target.value)
    }

    function changeWaktuSelesai(e) {
        e.preventDefault();
        setWaktuSelesai(e.target.value)
    }

    function changeDeskripsi(e) {
        e.preventDefault();
        setDeskripsi(e.target.value)
    }

    function save() {
        if (deskripsi === '' ||
            waktuMulai.length === 0 ||
            waktuSelesai.length === 0) {
            setWrongSubmit(true)
            setWrongTime(false)
        } else if (waktuMulai.split(':')[0] > waktuSelesai.split(':')[0] || 
        waktuMulai.split(':')[1] > waktuSelesai.split(':')[1] || 
        waktuMulai.split(':')[2] > waktuSelesai.split(':')[2]) {
            setWrongTime(true)
            setWrongSubmit(false)
        } else {
            setWrongSubmit(false)
            setWrongTime(false)
            let logAsisten = {
                idMataKuliah: idMataKuliah,
                npmMahasiswa: asdos[0].mahasiswa.npmMahasiswa,
                tanggalLogAsisten: selectedDate,
                bulanLogAsisten: selectedMonth,
                tahunLogAsisten: selectedYear,
                jamWaktuMulaiLogAsisten: parseInt(waktuMulai.split(':')[0], 10),
                menitWaktuMulaiLogAsisten: parseInt(waktuMulai.split(':')[1], 10),
                detikWaktuMulaiLogAsisten: parseInt(waktuMulai.split(':')[2], 10),
                jamWaktuSelesaiLogAsisten: parseInt(waktuSelesai.split(':')[0], 10),
                menitWaktuSelesaiLogAsisten: parseInt(waktuSelesai.split(':')[1], 10),
                detikWaktuSelesaiLogAsisten: parseInt(waktuSelesai.split(':')[2], 10),
                kategori: selectedKategori,
                deskripsiTugas: deskripsi,
            }

            let request = {
                logAsisten: logAsisten
            }
            LogAsistenService.updateLogAsisten(id, request);
            alert("File akan diupdate")
            history('/log-asisten');
        }
    }

    return (
        <>
            {(() => {
                if (principal !== null) {
                    return <div className="flex max-w-screen-lg w-full mx-auto gap-6 items-start font-['Montserrat']">
                        <div className="flex-1 p-8 bg-white rounded-lg shadow">
                            <h1 className="font-bold text-3xl m-5 underline decoration-[#780000] bg-[#EDEDED] text-[#0A0A0A] text-center">Update Log</h1>
                            <div>
                                <label className="text-gray-700 text-sm font-bold" htmlFor="username"> Id Mata Kuliah : </label>
                                <h1>{idMataKuliah}</h1>
                            </div>
                            <br></br>
                            <div>
                                <label className="text-gray-700 text-sm font-bold" htmlFor="username"> Kategori : </label>
                                <select className="form-control block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding
                                    border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none "
                                    value={selectedKategori} onChange={(e) => setSelectedKategori(e.target.value)}>
                                    {kategori.map((option, i) => (
                                        <option key={i} value={option.value}>{option.label}</option>
                                    ))}
                                </select>
                            </div>
                            <br></br>
                            <div className="flex flex-col">
                            <h1 className="text-gray-700 text-sm font-bold">Bulan, Tanggal, Tahun</h1>
                                <table className="table-auto">
                                    <tbody>
                                        <tr>
                                            <td colSpan='2'>
                                                <select className="form-control block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding
                                                    border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none "
                                                    value={selectedMonth} onChange={(e) => setSelectedMonth(e.target.value)}>
                                                    {month.map((option, i) => (
                                                        <option key={i} value={option.value}>{option.label}</option>
                                                    ))}
                                                </select>
                                            </td>

                                            <td>
                                                <select className="form-control block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding
                                                    border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none "
                                                    value={selectedDate} onChange={(e) => setSelectedDate(e.target.value)}>
                                                    {date.map((option, i) => (
                                                        <option key={i} value={option.value}>{option.label}</option>
                                                    ))}
                                                </select>
                                            </td>

                                            <td>
                                                <select className="form-control block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding
                                                    border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none "
                                                    value={selectedYear} onChange={(e) => setSelectedYear(e.target.value)}>
                                                    {year.map((option, i) => (
                                                        <option key={i} value={option.value}>{option.label}</option>
                                                    ))}
                                                </select>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <br></br>
                            <div>
                                <label className="text-gray-700 text-sm font-bold" htmlFor="deskripsi"> Deskripsi </label>
                                <br></br>
                                <textarea
                                    className="form-control block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding
                                    border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none "
                                    id="deskripsi"
                                    rows="3"
                                    onChange={(e) => changeDeskripsi(e)}
                                    value={deskripsi}
                                ></textarea>
                            </div>
                            <br></br>
                            <div>
                                <label className="text-gray-700 text-sm font-bold" htmlFor="waktuMulai"> Waktu Mulai : </label>
                                <br></br>
                                <input className="form-control block px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding
                                    border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                    onChange={(e) => changeWaktuMulai(e)} id="waktuMulai" type="time" step="1"
                                    value={waktuMulai} />
                            </div>
                            <br></br>
                            <div>
                                <label className="text-gray-700 text-sm font-bold" htmlFor="waktuSelesai"> Waktu Selesai : </label>
                                <br></br>
                                <input className="form-control block px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding
                                    border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none "
                                    onChange={(e) => changeWaktuSelesai(e)} id="waktuSelesai" type="time" step="1" value={waktuSelesai} />
                            </div>
                            {(() => {
                                if (wrongSubmit) {
                                    return <div className="flex flex-col gap-4">
                                        <h1 className="text-red-700 text-center">Semua Input Harus Diisi</h1>
                                    </div>
                                } else if (wrongTime) {
                                    return <div className="flex flex-col gap-4">
                                        <h1 className="text-red-700 text-center">Waktu Selesai Harus Di Set Setelah Waktu Mulai</h1>
                                    </div>
                                }
                            })()}
                            <div className="flex flex-col gap-4">
                                <button onClick={save}
                                    className="transition ease-in-out delay-150 space-y-1.0 mt-2.5 mx-80 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800">
                                    Simpan
                                </button>
                            </div>
                        </div>

                    </div>
                } else {
                    return <>
                        <div role="alert">
                            <div className="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                                Can't be Accessed!
                            </div>
                            <div className="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                                <p>You must have role ASISTEN DOSEN to access this page.</p>
                            </div>
                        </div>
                        <div className="flex flex-col justify-center items-center">
                            <button onClick={handleNavigate}
                                className="w-1/6 transition ease-in-out delay-50 space-y-1.0 mt-2.5 mx-80 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800">
                                Back to Home
                            </button>
                        </div>
                    </>
                }
            })()}
        </>
    )
}

export default UpdateLogAsistenComponent