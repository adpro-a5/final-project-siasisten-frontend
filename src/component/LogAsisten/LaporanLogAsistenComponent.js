import React, { useEffect, useState } from 'react'
import LaporanLogAsistenService from '../../services/LogAsisten/LaporanLogAsistenService';
import { useNavigate } from 'react-router-dom'
import Select from 'react-select';

const LaporanLogAsistenComponent = () => {
    const [allLaporan, setAllLaporan] = useState([])
    const [principal, setPrincipal] = useState({})
    const [asdos, setAsdos] = useState(null)
    const history = useNavigate();
    const [selectedYear, setSelectedYear] = useState('');
    const [selectedMonth, setSelectedMonth] = useState(null);
    const [selectedStatus, setSelectedStatus] = useState(null);
    const month = [
        { value: 'Januari', label: 'Januari' },
        { value: 'Februari', label: 'Februari' },
        { value: 'Maret', label: 'Maret' },
        { value: 'April', label: 'April' },
        { value: 'Mei', label: 'Mei' },
        { value: 'Juni', label: 'Juni' },
        { value: 'Juli', label: 'Juli' },
        { value: 'Agustus', label: 'Agustus' },
        { value: 'September', label: 'September' },
        { value: 'Oktober', label: 'Oktober' },
        { value: 'November', label: 'November' },
        { value: 'Desember', label: 'Desember' },
    ];

    const status = [
        { value: 'SEMUA', label: 'Semua' },
        { value: 'DILAPORKAN', label: 'Dilaporkan' },
        { value: 'DISETUJUI', label: 'Disetujui' },
        { value: 'TIDAK_DISETUJUI', label: 'Tidak Disetujui' },
        { value: 'DIPROSES', label: 'Diproses' },
        { value: 'SELESAI', label: 'Selesai' },
    ];
    const [totalJam, setTotalJam] = useState(0)
    const [totalPembayaran, setTotalPembayaran] = useState(0)

    useEffect(() => {
        const userData = localStorage.getItem("user")
        let user = ''
        if (typeof userData === 'object') {
            user = userData
        } else if (typeof userData === 'string') {
            user = JSON.parse(userData)
        }

        try {
            setPrincipal(user.principal)
        } catch (e) {
            setPrincipal(null)
        }
    }, []);

    useEffect(() => {
        if (principal !== null && principal.role === "MAHASISWA") { // Ubah
            const asdosData = sessionStorage.getItem("asdos")
            let asdos = ''
            if (typeof asdosData === 'object') {
                asdos = asdosData
            } else if (typeof asdosData === 'string') {
                asdos = JSON.parse(asdosData)
            }

            try {
                setAsdos(asdos)
            } catch (e) {
                setAsdos(null)
            }
        }
    }, [principal]);

    useEffect(() => {
        if (principal !== null && (principal.role === "ASISTENDOSEN" || principal.role === "KOORDINATOR")) { // Ubah
            const asdosData = localStorage.getItem("asdos")
            let asdos = ''
            if (typeof asdosData === 'object') {
                asdos = asdosData
            } else if (typeof asdosData === 'string') {
                asdos = JSON.parse(asdosData)
            }

            try {
                setAsdos(asdos)
            } catch (e) {
                setAsdos(null)
            }
        }
    }, [principal]);

    useEffect(() => {
        let jam = 0
        let pembayaran = 0
        console.log(allLaporan)
        for (let i = 0; i < allLaporan.length; i++) {
            jam = jam + allLaporan[i].jumlahJam
            pembayaran = pembayaran + allLaporan[i].jumlahPembayaran
        }
        setTotalJam(jam)
        setTotalPembayaran(pembayaran)
    }, [allLaporan]);

    function handleNavigate(e) {
        e.preventDefault();
        history('/mata-kuliah');
    }

    function getAllLaporan() {
        if (selectedMonth !== null && selectedYear !== '') {
            if (selectedStatus === null || selectedStatus.value === 'SEMUA') {
                const data = {
                    npmMahasiswa: asdos[0].mahasiswa.npmMahasiswa,
                    bulanLaporan: selectedMonth.value,
                    tahunLaporan: selectedYear
                }
                LaporanLogAsistenService.getAllLaporan(data).then((res) => {
                    setAllLaporan(res.data)
                })
            } else {
                const data = {
                    npmMahasiswa: asdos[0].mahasiswa.npmMahasiswa,
                    bulanLaporan: selectedMonth.value,
                    tahunLaporan: selectedYear,
                }
                LaporanLogAsistenService.getAllLaporanByStatus(selectedStatus.value, data).then((res) => {
                    setAllLaporan(res.data)
                })
            }
        } else {
            setAllLaporan([])
        }
    }

    return (
        <>
            {(() => {
                if (principal !== null) {
                    return <div className="flex max-w-screen-xl w-full mx-auto gap-6 items-start font-['Montserrat']">
                        <div className="flex-1 p-8 bg-white rounded-lg shadow">
                            <label className="text-gray-700 text-sm font-bold" htmlFor="tahun"> Tahun :  </label>
                            <input className="form-control block w-1/2 px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding
                                    border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                onChange={(e) => setSelectedYear(e.target.value)} id="tahun" />

                            <label className="text-gray-700 text-sm font-bold" htmlFor="bulan"> Bulan :  </label>
                            <Select
                                defaultValue={selectedMonth}
                                onChange={setSelectedMonth}
                                options={month}
                                className="w-1/2"
                            />

                            <label className="text-gray-700 text-sm font-bold" htmlFor="status"> Status :  </label>
                            <Select
                                defaultValue={selectedStatus}
                                onChange={setSelectedStatus}
                                options={status}
                                className="w-1/2"
                            />

                            <div className="flex space-x-0.5 m-5">
                                <div className="flex flex-col flex-1 gap-2">
                                    <button onClick={getAllLaporan}
                                        className="focus:outline-none text-white bg-emerald-700 hover:bg-emerald-800 focus:ring-4 focus:ring-emerald-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-emerald-600 dark:hover:bg-emerald-700 dark:focus:ring-emerald-800">
                                        Lihat Laporan
                                    </button>
                                </div>
                            </div>
                            <h1 className="font-bold text-3xl m-5 underline decoration-[#780000] bg-[#EDEDED] text-[#0A0A0A] text-center">Laporan Log Asisten</h1>
                            <div className="flex flex-col gap-4">
                                <table className="table-auto border-collapse border border-slate-500">
                                    <thead className="text-l uppercase bg-[#EDEDED] text-[#0A0A0A]">
                                        <tr>
                                            <th className='border border-slate-600'>NPM Mahasiswa</th>
                                            <th className='border border-slate-600'>Id Mata Kuliah</th>
                                            <th className='border border-slate-600'>Bulan - Tahun</th>
                                            <th className='border border-slate-600'>Jumlah Jam</th>
                                            <th className='border border-slate-600'>Honor Per Jam</th>
                                            <th className='border border-slate-600'>Jumlah Pembayaran</th>
                                            <th className='border border-slate-600'>Status</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        {
                                            allLaporan.map(
                                                (laporan, i) =>
                                                    <tr key={i} className="bg-white border-b dark:bg-gray-50 dark:border-gray-20 
                                                hover:bg-gray-50 dark:hover:bg-[#E0E0E0] dark:text-[#424242] whitespace-nowrap">
                                                        {(() => {
                                                            if (laporan.jumlahJam !== 0) {
                                                                return <>
                                                                    <td className='border border-slate-700'>
                                                                        <div className="text-center">
                                                                            {laporan.idMataKuliah}
                                                                        </div>
                                                                    </td>
                                                                    <td className='border border-slate-700'>
                                                                        <div className="text-center">
                                                                            {laporan.npmMahasiswa}
                                                                        </div>
                                                                    </td>
                                                                    <td className='border border-slate-700'>
                                                                        <div className="text-center">
                                                                            {laporan.bulanLaporanLogAsisten + ' - ' + laporan.tahunLaporanLogAsisten}
                                                                        </div>
                                                                    </td>
                                                                    <td className='border border-slate-700'>
                                                                        <div className="text-center">
                                                                            {laporan.jumlahJam}
                                                                        </div>
                                                                    </td>
                                                                    <td className='border border-slate-700'>
                                                                        <div className="text-center">
                                                                            {laporan.honorPerJam}
                                                                        </div>
                                                                    </td>
                                                                    <td className='border border-slate-700'>
                                                                        <div className="text-center">
                                                                            Rp.{laporan.jumlahPembayaran}
                                                                        </div>
                                                                    </td>
                                                                    <td className='border border-slate-700'>
                                                                        <div className="text-center">
                                                                            {laporan.statusLogAsisten}
                                                                        </div>
                                                                    </td>
                                                                </>
                                                            }
                                                        })()}
                                                    </tr>
                                            )
                                        }
                                        <tr className="bg-white border-b dark:bg-gray-50 dark:border-gray-20 
                                                hover:bg-gray-50 dark:hover:bg-[#E0E0E0] dark:text-[#424242] whitespace-nowrap">
                                            <td colSpan='3' className='border border-slate-700'>
                                                <div className="text-center">
                                                    TOTAL
                                                </div>
                                            </td>
                                            <td colSpan='2' className='border border-slate-700'>
                                                <div className="text-center">
                                                    {totalJam}
                                                </div>
                                            </td>
                                            <td colSpan='2' className='border border-slate-700'>
                                                <div className="text-center">
                                                    Rp.{totalPembayaran}
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                } else {
                    return <>
                        <div role="alert">
                            <div className="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                                Can't be Accessed!
                            </div>
                            <div className="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                                <p>You must have role ADMIN to access this page.</p>
                            </div>
                        </div>
                        <div className="flex flex-col justify-center items-center">
                            <button onClick={handleNavigate}
                                className="w-1/6 transition ease-in-out delay-50 space-y-1.0 mt-2.5 mx-80 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800">
                                Back to Home
                            </button>
                        </div>
                    </>
                }
            })()}
        </>
    )
}

export default LaporanLogAsistenComponent