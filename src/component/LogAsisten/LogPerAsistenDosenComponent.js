import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import StatusLogAsistenService from '../../services/LogAsisten/StatusLogAsistenService'

const LogPerAsistenDosenComponent = () => {
    const [log, setLog] = useState([null])
    const [principal, setPrincipal] = useState({})
    const { id, npm } = useParams('')

    useEffect(() => {
        let request = {
            npmMahasiswa: npm
        }
        StatusLogAsistenService.getAllLogAsistenByIdMataKuliah(id, request).then((res) => {
            console.log(res.data)
            setLog(res.data)
        });
    }, [id, npm]);

    function actionLog(e, id, action) {
        let request = {
            statusLogAsisten: action
        }

        try {
            StatusLogAsistenService.updateLogAsisten(id, request);
        } catch (e) {
            alert(e)
        }
    }

    useEffect(() => {
        const userData = localStorage.getItem("user")
        let user = ''
        if (typeof userData === 'object') {
            user = userData
        } else if (typeof userData === 'string') {
            user = JSON.parse(userData)
        }

        try {
            setPrincipal(user.principal)
        } catch (e) {
            setPrincipal(null)
        }
    }, []);

    return (
        <>
            {(() => {
                if (principal !== null) {
                    if (log.length === 1 & log[0] === null) {
                        return <div className="divLoader">
                            <svg className="svgLoader" viewBox="0 0 100 100" width="10em" height="10em">
                                <path ng-attr-d="{{config.pathCmd}}" ng-attr-fill="{{config.color}}" stroke="none" d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#51CACC" transform="rotate(179.719 50 51)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 51;360 50 51" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></path>
                            </svg>
                        </div>
                    } else {
                        return <div className="flex max-w-screen-xl w-full mx-auto gap-6 items-start font-['Montserrat']">
                            <div className="flex-1 p-8 bg-white rounded-lg shadow">
                                <h1 className="font-bold text-3xl m-5 underline decoration-[#780000] bg-[#EDEDED] text-[#0A0A0A] text-center">Log Asisten Mahasiswa - {npm}</h1>
                                <div className="flex flex-col gap-4">
                                    <table className="table-auto">
                                        <thead className="text-l uppercase bg-[#EDEDED] text-[#0A0A0A]">
                                            <tr>
                                                <th>Id Mata Kuliah</th>
                                                <th>Tanggal</th>
                                                <th>Jam</th>
                                                <th>Kategori</th>
                                                <th colSpan="5">Deskripsi Tugas</th>
                                                <th>Status</th>
                                                <th>Operation</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            {
                                                log.map(
                                                    (log, i) =>
                                                        <tr key={i} className="bg-white border-b dark:bg-gray-50 dark:border-gray-20 
                            hover:bg-gray-50 dark:hover:bg-[#E0E0E0] dark:text-[#424242] whitespace-nowrap">
                                                            <td>
                                                                <div className="text-center">
                                                                    {log.idMataKuliah}
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="text-center">
                                                                    {log.tanggalLogAsisten < 10 ? '0' + log.tanggalLogAsisten : log.tanggalLogAsisten}{' '}
                                                                    {log.bulanLogAsisten}{' '}
                                                                    {log.tahunLogAsisten < 10 ? '000' + log.tahunLogAsisten : log.tahunLogAsisten < 100 ? '00' + log.tahunLogAsisten : log.tahunLogAsisten < 1000 ? '0' + log.tahunLogAsisten : log.tahunLogAsisten}
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="text-center">
                                                                    {log.jamWaktuMulaiLogAsisten < 10 ? '0' + log.jamWaktuMulaiLogAsisten : log.jamWaktuMulaiLogAsisten}:
                                                                    {log.menitWaktuMulaiLogAsisten < 10 ? '0' + log.menitWaktuMulaiLogAsisten : log.menitWaktuMulaiLogAsisten}:
                                                                    {log.detikWaktuMulaiLogAsisten < 10 ? '0' + log.detikWaktuMulaiLogAsisten : log.detikWaktuMulaiLogAsisten} {' - '}
                                                                    {log.jamWaktuSelesaiLogAsisten < 10 ? '0' + log.jamWaktuSelesaiLogAsisten : log.jamWaktuSelesaiLogAsisten}:
                                                                    {log.menitWaktuSelesaiLogAsisten < 10 ? '0' + log.menitWaktuSelesaiLogAsisten : log.menitWaktuSelesaiLogAsisten}:
                                                                    {log.detikWaktuSelesaiLogAsisten < 10 ? '0' + log.detikWaktuSelesaiLogAsisten : log.detikWaktuSelesaiLogAsisten}
                                                                </div>
                                                                <div className="text-center">
                                                                    {log.jamTotalWaktuLogAsisten}{' Jam '}{log.menitTotalWaktuLogAsisten}{' Menit '}{log.detikTotalWaktuLogAsisten}{' Detik '}
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="text-center">
                                                                    {log.kategori}
                                                                </div>
                                                            </td>
                                                            <td colSpan="5">
                                                                <div className="text-center">
                                                                    {log.deskripsiTugas}
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="text-center">
                                                                    {log.statusLogAsisten}
                                                                </div>
                                                            </td>
                                                            <td className="flex space-x-0.5 mt-1.5">
                                                                {(() => {
                                                                    if (log.statusLogAsisten === 'DILAPORKAN') {
                                                                        return (<>
                                                                            <div className="flex flex-col flex-1 gap-2">
                                                                                <button onClick={(e) => actionLog(e, log.id, "DISETUJUI")}
                                                                                    className="focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800">
                                                                                    SETUJUI
                                                                                </button>
                                                                            </div>
                                                                            <div className="flex flex-col flex-1 gap-2">
                                                                                <button onClick={(e) => actionLog(e, log.id, "TIDAK_DISETUJUI")}
                                                                                    className="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800">
                                                                                    TOLAK
                                                                                </button>
                                                                            </div>
                                                                        </>
                                                                        )
                                                                    } else if (log.statusLogAsisten === 'DISETUJUI') {
                                                                        return <div className="flex flex-col flex-1 gap-2">
                                                                            <button onClick={(e) => actionLog(e, log.id, "DIPROSES")}
                                                                                className="focus:outline-none text-white bg-yellow-700 hover:bg-yellow-800 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-yellow-600 dark:hover:bg-yellow-700 dark:focus:ring-yellow-800">
                                                                                PROSES
                                                                            </button>
                                                                        </div>
                                                                    } else if (log.statusLogAsisten === 'DIPROSES') {
                                                                        return <div className="flex flex-col flex-1 gap-2">
                                                                            <button onClick={(e) => actionLog(e, log.id, "SELESAI")}
                                                                                className="focus:outline-none text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                                                                SELESAI
                                                                            </button>
                                                                        </div>
                                                                    }
                                                                })()}
                                                            </td>
                                                        </tr>
                                                )
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    }
                }
            })()}

        </>
    )
}

export default LogPerAsistenDosenComponent