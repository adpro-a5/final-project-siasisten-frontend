import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import Select from 'react-select';
import LogAsistenService from '../../services/LogAsisten/LogAsistenService'

const CreateLogAsistenComponent = () => {
    const [principal, setPrincipal] = useState({})
    const [asdos, setAsdos] = useState(null)

    const [selectedMatkul, setSelectedMatkul] = useState(null);
    const [selectedDate, setSelectedDate] = useState(null);
    const [selectedMonth, setSelectedMonth] = useState(null);
    const [selectedYear, setSelectedYear] = useState(null);
    const [waktuMulai, setWaktuMulai] = useState([])
    const [waktuSelesai, setWaktuSelesai] = useState([])
    const [selectedKategori, setSelectedKategori] = useState(null);
    const [deskripsi, setDeskripsi] = useState('')

    const [wrongSubmit, setWrongSubmit] = useState(false)
    const [wrongTime, setWrongTime] = useState(false)

    const [matkul, setMatkul] = useState([]);
    const date = [
        { value: 1, label: '01' },
        { value: 2, label: '02' },
        { value: 3, label: '03' },
        { value: 4, label: '04' },
        { value: 5, label: '05' },
        { value: 6, label: '06' },
        { value: 7, label: '07' },
        { value: 8, label: '08' },
        { value: 9, label: '09' },
        { value: 10, label: '10' },
        { value: 11, label: '11' },
        { value: 12, label: '12' },
        { value: 14, label: '13' },
        { value: 14, label: '14' },
        { value: 15, label: '15' },
        { value: 16, label: '16' },
        { value: 17, label: '17' },
        { value: 18, label: '18' },
        { value: 19, label: '19' },
        { value: 20, label: '20' },
        { value: 21, label: '21' },
        { value: 22, label: '22' },
        { value: 23, label: '23' },
        { value: 24, label: '24' },
        { value: 25, label: '25' },
        { value: 26, label: '26' },
        { value: 27, label: '27' },
        { value: 28, label: '28' },
        { value: 29, label: '29' },
        { value: 30, label: '30' },
        { value: 31, label: '31' },
    ];    
    const month = [
        { value: 'Januari', label: 'Januari' },
        { value: 'Februari', label: 'Februari' },
        { value: 'Maret', label: 'Maret' },
        { value: 'April', label: 'April' },
        { value: 'Mei', label: 'Mei' },
        { value: 'Juni', label: 'Juni' },
        { value: 'Juli', label: 'Juli' },
        { value: 'Agustus', label: 'Agustus' },
        { value: 'September', label: 'September' },
        { value: 'Oktober', label: 'Oktober' },
        { value: 'November', label: 'November' },
        { value: 'Desember', label: 'Desember' },
    ];
    const year = [
        { value: 2020, label: '2020' },
        { value: 2021, label: '2021' },
        { value: 2022, label: '2022' },
    ];
    const kategori = [
        { value: 'Asistensi/Tutorial', label: 'Asistensi/Tutorial' },
        { value: 'Mengoreksi', label: 'Mengoreksi' },
        { value: 'Mengawas', label: 'Mengawas' },
        { value: 'Persiapan Asistensi', label: 'Persiapan Asistensi' },
        { value: 'Membuat soal/tugas', label: 'Membuat soal/tugas' },
        { value: 'Rapat', label: 'Rapat' },
        { value: 'Sit in Kelas', label: 'Sit in Kelas' },
        { value: 'Pengembangan Materi', label: 'Pengembangan Materi' },
        { value: 'Pengembangan apps', label: 'Pengembangan apps' },
        { value: 'Persiapan infra', label: 'Persiapan infra' },
        { value: 'Dokumentasi', label: 'Dokumentasi' },
        { value: 'Persiapan kuliah', label: 'Persiapan kuliah' },
        { value: 'Penunjang', label: 'Penunjang' },
        { value: 'Input Data', label: 'Input Data' },
    ];

    const history = useNavigate();

    useEffect(() => {
        if (asdos !== null) {
            const opt = []
            for (let i = 0; i < asdos.length; i++) {
                let a = {
                    value: asdos[i].mataKuliah.idMataKuliah,
                    label: asdos[i].mataKuliah.namaMataKuliah
                }
                opt.push(a)
            }
            setMatkul(opt)
        }
    }, [asdos]);

    useEffect(() => {
        const userData = localStorage.getItem("user")
        let user = ''
        if (typeof userData === 'object') {
            user = userData
        } else if (typeof userData === 'string') {
            user = JSON.parse(userData)
        }

        try {
            setPrincipal(user.principal)
        } catch (e) {
            setPrincipal(null)
        }
    }, []);

    useEffect(() => {
        if (principal !== null && (principal.role === "ASISTENDOSEN" || principal.role === "KOORDINATOR")) { // Ubah
            const asdosData = localStorage.getItem("asdos")
            let asdos = ''
            if (typeof asdosData === 'object') {
                asdos = asdosData
            } else if (typeof asdosData === 'string') {
                asdos = JSON.parse(asdosData)
            }

            try {
                setAsdos(asdos)
            } catch (e) {
                setAsdos(null)
            }
        }
    }, [principal]);

    function handleNavigate() {
        history('/mata-kuliah');
    }

    function changeWaktuMulai(e) {
        e.preventDefault();
        setWaktuMulai(e.target.value.split(':'))
    }

    function changeWaktuSelesai(e) {
        e.preventDefault();
        setWaktuSelesai(e.target.value.split(':'))
    }

    function changeDeskripsi(e) {
        e.preventDefault();
        setDeskripsi(e.target.value)
    }

    function save() {
        if (selectedMatkul === null || 
            selectedKategori === null ||
            selectedDate === null ||
            selectedMonth === null || 
            selectedYear === null ||
            deskripsi === '' ||
            waktuMulai.length === 0 ||
            waktuSelesai.length === 0) {
            setWrongSubmit(true)
            setWrongTime(false)
        } else if (waktuMulai[0] > waktuSelesai[0] || waktuMulai[1] > waktuSelesai[1] || waktuMulai[2] > waktuSelesai[2]) {
            setWrongTime(true)
            setWrongSubmit(false)
        } else {
            setWrongSubmit(false)
            setWrongTime(false)
            let logAsisten = {
                idMataKuliah: selectedMatkul.value,
                npmMahasiswa: asdos[0].mahasiswa.npmMahasiswa,
                tanggalLogAsisten: selectedDate.value,
                bulanLogAsisten: selectedMonth.value,
                tahunLogAsisten: selectedYear.value,
                jamWaktuMulaiLogAsisten: parseInt(waktuMulai[0], 10),
                menitWaktuMulaiLogAsisten: parseInt(waktuMulai[1], 10),
                detikWaktuMulaiLogAsisten: parseInt(waktuMulai[2], 10),
                jamWaktuSelesaiLogAsisten: parseInt(waktuSelesai[0], 10),
                menitWaktuSelesaiLogAsisten: parseInt(waktuSelesai[1], 10),
                detikWaktuSelesaiLogAsisten: parseInt(waktuSelesai[2], 10),
                kategori: selectedKategori.value,
                deskripsiTugas: deskripsi,
            } 

            let request = {
                logAsisten: logAsisten
            }
            console.log(request)
            LogAsistenService.createLogAsisten(request);
            history('/log-asisten');
        }
    }

    return (
        <>
            {(() => {
                if (principal !== null) {
                    return <div className="flex max-w-screen-lg w-full mx-auto gap-6 items-start font-['Montserrat']">
                        <div className="flex-1 p-8 bg-white rounded-lg shadow">
                            <h1 className="font-bold text-3xl m-5 underline decoration-[#780000] bg-[#EDEDED] text-[#0A0A0A] text-center">Tambah Log</h1>
                            <div>
                                <label className="text-gray-700 text-sm font-bold" htmlFor="username"> Mata Kuliah : </label>
                                <Select
                                    defaultValue={selectedMatkul}
                                    onChange={setSelectedMatkul}
                                    options={matkul}
                                />
                            </div>
                            <br></br>
                            <div>
                                <label className="text-gray-700 text-sm font-bold" htmlFor="username"> Kategori : </label>
                                <Select
                                    defaultValue={selectedKategori}
                                    onChange={setSelectedKategori}
                                    options={kategori}
                                />
                            </div>
                            <br></br>
                            <div className="flex flex-col gap-4">
                                <table className="table-auto">
                                    <label className="text-gray-700 text-sm font-bold" htmlFor="username"> Bulan, Tanggal, Tahun : </label>
                                    <tbody>
                                        <tr>
                                            <td colSpan='2'>
                                                <Select
                                                    defaultValue={selectedMonth}
                                                    onChange={setSelectedMonth}
                                                    options={month}
                                                />
                                            </td>
                                            <td>
                                                <Select
                                                    defaultValue={selectedDate}
                                                    onChange={setSelectedDate}
                                                    options={date}
                                                />
                                            </td>
                                            <td>
                                                <Select
                                                    defaultValue={selectedYear}
                                                    onChange={setSelectedYear}
                                                    options={year}
                                                />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <br></br>
                            <div>
                                <label className="text-gray-700 text-sm font-bold" htmlFor="deskripsi"> Deskripsi </label>
                                <br></br>
                                <textarea
                                    className="form-control block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding
                                    border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none "
                                    id="deskripsi"
                                    rows="3"
                                    onChange={(e) => changeDeskripsi(e)}
                                ></textarea>
                            </div>
                            <br></br>
                            <div>
                                <label className="text-gray-700 text-sm font-bold" htmlFor="waktuMulai"> Waktu Mulai : </label>
                                <br></br>
                                <input className="form-control block px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding
                                    border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                    onChange={(e) => changeWaktuMulai(e)} id="waktuMulai" type="time" step="1" />
                            </div>
                            <br></br>
                            <div>
                                <label className="text-gray-700 text-sm font-bold" htmlFor="waktuSelesai"> Waktu Selesai : </label>
                                <br></br>
                                <input className="form-control block px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding
                                    border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none "
                                    onChange={(e) => changeWaktuSelesai(e)} id="waktuSelesai" type="time" step="1" />
                            </div>
                            {(() => {
                                if (wrongSubmit) {
                                    return <div className="flex flex-col gap-4">
                                        <h1 className="text-red-700 text-center">Semua Input Harus Diisi</h1>
                                    </div>
                                } else if (wrongTime) {
                                    return <div className="flex flex-col gap-4">
                                        <h1 className="text-red-700 text-center">Waktu Selesai Harus Di Set Setelah Waktu Mulai</h1>
                                    </div>
                                }
                            })()}
                            <div className="flex flex-col gap-4">
                                <button onClick={save}
                                    className="transition ease-in-out delay-150 space-y-1.0 mt-2.5 mx-80 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800">
                                    Simpan
                                </button>
                            </div>
                        </div>

                    </div>
                } else {
                    return <>
                        <div role="alert">
                            <div className="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                                Can't be Accessed!
                            </div>
                            <div className="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                                <p>You must have role ASISTEN DOSEN to access this page.</p>
                            </div>
                        </div>
                        <div className="flex flex-col justify-center items-center">
                            <button onClick={handleNavigate}
                                className="w-1/6 transition ease-in-out delay-50 space-y-1.0 mt-2.5 mx-80 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800">
                                Back to Home
                            </button>
                        </div>
                    </>
                }
            })()}
        </>
    )
}

export default CreateLogAsistenComponent