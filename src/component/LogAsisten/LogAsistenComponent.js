import React, { useEffect, useState } from 'react'
import LogAsistenService from '../../services/LogAsisten/LogAsistenService';
import AllLogAsistenService from '../../services/LogAsisten/AllLogAsistenService';
import { useNavigate } from 'react-router-dom'
import Select from 'react-select';

const LogAsistenComponent = () => {
    const [isAll, setIsAll] = useState(true)
    const [allLog, setAllLog] = useState([null])
    const [matkulLog, setMatkulLog] = useState([null])
    const [log, setLog] = useState([null])
    const [principal, setPrincipal] = useState({})
    const [asdos, setAsdos] = useState(null)
    const history = useNavigate();
    const [selectedOption, setSelectedOption] = useState(null);
    const [options, setOptions] = useState([]);

    useEffect(() => {
        if (asdos !== null) {
            async function fetchMyAPI() {
                const npm = {
                    npmMahasiswa: asdos[0].mahasiswa.npmMahasiswa
                }
                await AllLogAsistenService.getAllLogAsisten(npm).then((res) => {
                    setAllLog(res.data)
                    setLog(res.data)
                });
            }
            fetchMyAPI();

            const opt = []
            for (let i = 0; i < asdos.length; i++) {
                let a = {
                    value: asdos[i].mataKuliah.idMataKuliah,
                    label: asdos[i].mataKuliah.namaMataKuliah
                }
                opt.push(a)
            }
            setOptions(opt)
        }
    }, [asdos]);

    useEffect(() => {
        const userData = localStorage.getItem("user")
        let user = ''
        if (typeof userData === 'object') {
            user = userData
        } else if (typeof userData === 'string') {
            user = JSON.parse(userData)
        }

        try {
            setPrincipal(user.principal)
        } catch (e) {
            setPrincipal(null)
        }
    }, []);

    useEffect(() => {
        if (isAll) {
            setLog(allLog)
        } else if (selectedOption !== null) {
            setLog(matkulLog)
        }
    }, [isAll, allLog, matkulLog, selectedOption]);

    useEffect(() => {
        if (selectedOption !== null) {
            async function fetchMyAPI() {
                const npm = {
                    npmMahasiswa: asdos[0].mahasiswa.npmMahasiswa
                }
                await AllLogAsistenService.getAllLogAsistenByIdMataKuliah(npm, selectedOption.value).then((res) => {
                    setMatkulLog(res.data)
                });
            }
            fetchMyAPI();
        }
    }, [selectedOption, asdos]);

    useEffect(() => {
        if (principal !== null && (principal.role === "ASISTENDOSEN" || principal.role === "KOORDINATOR")) { // Ubah
            const asdosData = localStorage.getItem("asdos")
            let asdos = ''
            if (typeof asdosData === 'object') {
                asdos = asdosData
            } else if (typeof asdosData === 'string') {
                asdos = JSON.parse(asdosData)
            }

            try {
                setAsdos(asdos)
            } catch (e) {
                setAsdos(null)
            }
        }
    }, [principal]);

    function handleNavigate(e) {
        e.preventDefault();
        history('/mata-kuliah');
    }

    function addLogAsisten(e) {
        e.preventDefault();
        history('/log-asisten/add-log');
    }

    function updateLog(e, id) {
        e.preventDefault();
        history('/log-asisten/update-log/' + id);
    }

    function deleteLog(e, id) {
        e.preventDefault();
        LogAsistenService.deleteLogAsisten(id);
        alert("File akan dihapus")
        window.location.reload();
    }
    
    const allMataKuliah = (e) => {
        e.preventDefault();
        setIsAll(true)
    }

    const filterMataKuliah = (e) => {
        e.preventDefault();
        setIsAll(false)
    }

    return (
        <>
            {(() => {
                if (principal !== null) {
                    if (log.length === 1 & log[0] === null) {
                        return <div className="divLoader">
                            <svg className="svgLoader" viewBox="0 0 100 100" width="10em" height="10em">
                                <path ng-attr-d="{{config.pathCmd}}" ng-attr-fill="{{config.color}}" stroke="none" d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#51CACC" transform="rotate(179.719 50 51)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 51;360 50 51" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></path>
                            </svg>
                        </div>
                    } else {
                        return <div className="flex max-w-screen-xl w-full mx-auto gap-6 items-start font-['Montserrat']">
                            <div className="flex-1 p-8 bg-white rounded-lg shadow">
                                <label className="text-gray-700 text-sm font-bold" htmlFor="username"> Filter Mata Kuliah </label>
                                <Select
                                    defaultValue={selectedOption}
                                    onChange={setSelectedOption}
                                    options={options}
                                />

                                <div className="flex space-x-0.5 m-5">
                                    <div className="flex flex-col flex-1 gap-2">
                                        <button onClick={(e) => allMataKuliah(e)}
                                            className="focus:outline-none text-white bg-emerald-700 hover:bg-emerald-800 focus:ring-4 focus:ring-emerald-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-emerald-600 dark:hover:bg-emerald-700 dark:focus:ring-emerald-800">
                                            Cek Semua Log Asisten
                                        </button>
                                    </div>
                                    <div className="flex flex-col flex-1 gap-2">
                                        <button onClick={(e) => filterMataKuliah(e)}
                                            className="focus:outline-none text-white bg-teal-700 hover:bg-teal-800 focus:ring-4 focus:ring-teal-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-teal-600 dark:hover:bg-teal-700 dark:focus:ring-teal-800">
                                            Cek Log Asisten berdasarkan Filter
                                        </button>
                                    </div>
                                    <div className="flex flex-col flex-1 gap-2">
                                        <button onClick={(e) => addLogAsisten(e)}
                                            className="focus:outline-none text-white bg-cyan-700 hover:bg-cyan-800 focus:ring-4 focus:ring-cyan-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-cyan-600 dark:hover:bg-cyan-700 dark:focus:ring-cyan-800">
                                            Tambah Log Asisten
                                        </button>
                                    </div>
                                </div>
                                <h1 className="font-bold text-3xl m-5 underline decoration-[#780000] bg-[#EDEDED] text-[#0A0A0A] text-center">Log Asisten - {isAll ? 'All Mata Kuliah' : selectedOption === null ? 'All Mata Kuliah' : 'Mata Kuliah ' + selectedOption.label}</h1>
                                <div className="flex flex-col gap-4">
                                    <table className="table-auto">
                                        <thead className="text-l uppercase bg-[#EDEDED] text-[#0A0A0A]">
                                            <tr>
                                                <th>Id Mata Kuliah</th>
                                                <th>Tanggal</th>
                                                <th>Jam</th>
                                                <th>Kategori</th>
                                                <th colSpan="5">Deskripsi Tugas</th>
                                                <th>Status</th>
                                                <th>Operation</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            {
                                                log.map(
                                                    (log, i) =>
                                                        <tr key={i} className="bg-white border-b dark:bg-gray-50 dark:border-gray-20 
                                    hover:bg-gray-50 dark:hover:bg-[#E0E0E0] dark:text-[#424242] whitespace-nowrap">
                                                            <td>
                                                                <div className="text-center">
                                                                    {log.idMataKuliah}
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="text-center">
                                                                    {log.tanggalLogAsisten < 10 ? '0' + log.tanggalLogAsisten : log.tanggalLogAsisten}{' '}
                                                                    {log.bulanLogAsisten}{' '}
                                                                    {log.tahunLogAsisten < 10 ? '000' + log.tahunLogAsisten : log.tahunLogAsisten < 100 ? '00' + log.tahunLogAsisten : log.tahunLogAsisten < 1000 ? '0' + log.tahunLogAsisten : log.tahunLogAsisten}
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="text-center">
                                                                    {log.jamWaktuMulaiLogAsisten < 10 ? '0' + log.jamWaktuMulaiLogAsisten : log.jamWaktuMulaiLogAsisten}:
                                                                    {log.menitWaktuMulaiLogAsisten < 10 ? '0' + log.menitWaktuMulaiLogAsisten : log.menitWaktuMulaiLogAsisten}:
                                                                    {log.detikWaktuMulaiLogAsisten < 10 ? '0' + log.detikWaktuMulaiLogAsisten : log.detikWaktuMulaiLogAsisten} {' - '}
                                                                    {log.jamWaktuSelesaiLogAsisten < 10 ? '0' + log.jamWaktuSelesaiLogAsisten : log.jamWaktuSelesaiLogAsisten}:
                                                                    {log.menitWaktuSelesaiLogAsisten < 10 ? '0' + log.menitWaktuSelesaiLogAsisten : log.menitWaktuSelesaiLogAsisten}:
                                                                    {log.detikWaktuSelesaiLogAsisten < 10 ? '0' + log.detikWaktuSelesaiLogAsisten : log.detikWaktuSelesaiLogAsisten}
                                                                </div>
                                                                <div className="text-center">
                                                                    {log.jamTotalWaktuLogAsisten}{' Jam '}{log.menitTotalWaktuLogAsisten}{' Menit '}{log.detikTotalWaktuLogAsisten}{' Detik '}
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="text-center">
                                                                    {log.kategori}
                                                                </div>
                                                            </td>
                                                            <td colSpan="5">
                                                                <div className="text-center">
                                                                    {log.deskripsiTugas}
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="text-center">
                                                                    {log.statusLogAsisten}
                                                                </div>
                                                            </td>
                                                            <td className="flex space-x-0.5 mt-1.5">
                                                                {(() => {
                                                                    if (log.statusLogAsisten === 'DILAPORKAN') {
                                                                        return (
                                                                            <>
                                                                                <div className="flex flex-col flex-1 gap-2">
                                                                                    <button onClick={(e) => updateLog(e, log.id)}
                                                                                        className="focus:outline-none text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                                                                        Ubah
                                                                                    </button>
                                                                                </div>
                                                                                <div className="flex flex-col flex-1 gap-2">
                                                                                    <button onClick={(e) => deleteLog(e, log.id)}
                                                                                        className="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800">
                                                                                        Delete
                                                                                    </button>
                                                                                </div>
                                                                            </>
                                                                        )
                                                                    }
                                                                })()}
                                                            </td>
                                                        </tr>
                                                )
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    }
                } else {
                    return <>
                        <div role="alert">
                            <div className="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                                Can't be Accessed!
                            </div>
                            <div className="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                                <p>You must have role ADMIN to access this page.</p>
                            </div>
                        </div>
                        <div className="flex flex-col justify-center items-center">
                            <button onClick={handleNavigate}
                                className="w-1/6 transition ease-in-out delay-50 space-y-1.0 mt-2.5 mx-80 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800">
                                Back to Home
                            </button>
                        </div>
                    </>
                }
            })()}
        </>
    )
}

export default LogAsistenComponent