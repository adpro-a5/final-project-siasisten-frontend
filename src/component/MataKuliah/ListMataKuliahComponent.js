import React, { useEffect, useState } from 'react'
import MataKuliahService from '../../services/MataKuliah/MataKuliahService'
import DosenService from '../../services/Pengguna/DosenService'
import { useNavigate } from 'react-router-dom';

const ListMataKuliahComponent = () => {
    const [principal, setPrincipal] = useState({})
    const [mataKuliahs, setMataKuliahs] = useState([])
    const [dosen, setDosen] = useState([])
    const [err, serErr] = useState(null)
    const history = useNavigate();

    function handleNavigate(e) {
        e.preventDefault();
        history('/add-mata-kuliah');
    }

    useEffect(() => {
        async function fetchMyAPI() {
            const userData = localStorage.getItem("user")
            let user = ''
            if (typeof userData === 'object') {
                user = userData
            } else if (typeof userData === 'string') {
                user = JSON.parse(userData)
            }

            try {
                setPrincipal(user.principal)
            } catch (e) {
                setPrincipal(null)
            }

            if (user.principal !== null && user.principal.role === 'DOSEN') {
                await DosenService.getDosenByEmail(user.principal.email).then((res) => {
                    setDosen(res.data)
                })
            }
        }

        fetchMyAPI();
    }, []);

    useEffect(() => {
        MataKuliahService.getMataKuliah().then((res) => {
            setMataKuliahs(res.data)
        }).catch((error) => {
            serErr(error)
        });
    }, []);

    function daftarAsistenDosen(idMataKuliah, namaMataKuliah) {
        history("/pendaftaran/" + idMataKuliah + "/" + namaMataKuliah)
    }

    function detailAsistenDosen(idMataKuliah, namaMataKuliah) {
        history("/asisten-dosen/" + idMataKuliah + "/" + namaMataKuliah)
    }

    return (<>
        {(() => {
            if (err === null) {
                if (mataKuliahs.length !== 0) {
                    return <div className="flex max-w-screen-lg w-full mx-auto gap-6 items-start font-['Montserrat']">
                        <div className="flex-1 p-8 bg-white rounded-lg shadow">
                            <h1 className="font-bold text-3xl mb-9 underline decoration-[#780000] bg-[#EDEDED] text-[#0A0A0A] text-center">Daftar Mata Kuliah</h1>
                            <div className="flex flex-col gap-4">
                                <table className="table-auto">
                                    <thead className="text-l uppercase bg-[#EDEDED] text-[#0A0A0A]">
                                        <tr>
                                            <th>ID</th>
                                            <th>Mata Kuliah</th>
                                            <th>Jumlah Lowongan</th>
                                            <th>Jumlah Pelamar</th>
                                            <th>Jumlah Diterima</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        {
                                            mataKuliahs.map(
                                                mataKuliah =>
                                                    <tr key={mataKuliah.idMataKuliah} className="bg-white border-b dark:bg-gray-50 dark:border-gray-20 
                                        hover:bg-gray-50 dark:hover:bg-[#E0E0E0] dark:text-[#424242] whitespace-nowrap">
                                                        <td>
                                                            <div className="text-center">
                                                                {mataKuliah.idMataKuliah}
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div className="text-center">
                                                                {mataKuliah.namaMataKuliah}
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div className="text-center">
                                                                {mataKuliah.jumlahLowongan}
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div className="text-center">
                                                                {mataKuliah.jumlahPelamar}
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div className="text-center">
                                                                {mataKuliah.jumlahAsisten}
                                                            </div>
                                                        </td>
                                                        <td className="flex space-x-0.5 mt-1.5">
                                                            {(() => {
                                                                if (principal != null) {
                                                                    return <>
                                                                        {(() => {
                                                                            if (principal.role === 'MAHASISWA' || principal.role === 'ASISTENDOSEN' || principal.role === 'KOORDINATOR') {
                                                                                return <div className="flex flex-col flex-1 gap-2">
                                                                                    <button onClick={() => daftarAsistenDosen(mataKuliah.idMataKuliah, mataKuliah.namaMataKuliah)}
                                                                                        className="focus:outline-none text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-900">
                                                                                        Daftar
                                                                                    </button>
                                                                                </div>
                                                                            } else {
                                                                                return <div className="flex flex-col flex-1 gap-2">
                                                                                    <button onClick={() => detailAsistenDosen(mataKuliah.idMataKuliah, mataKuliah.namaMataKuliah)}
                                                                                        className="focus:outline-none text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-0.5 py-0.5 mr-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-900">
                                                                                        Detail
                                                                                    </button>
                                                                                </div>
                                                                            }
                                                                        })()}
                                                                    </>
                                                                }
                                                            })()}
                                                        </td>
                                                    </tr>
                                            )
                                        }
                                    </tbody>
                                </table>
                                {(() => {
                                    if (principal !== null) {
                                        return <>
                                            {(() => {
                                                if (principal.role === 'ADMIN') {
                                                    return <>
                                                        <button onClick={handleNavigate}
                                                            className="transition ease-in-out delay-150 space-y-1.0 mt-2.5 mx-80 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800">
                                                            Add Mata Kuliah
                                                        </button>
                                                    </>
                                                } else if (principal.role === 'DOSEN' && dosen.isApprove) {
                                                    return <>
                                                        <button onClick={handleNavigate}
                                                            className="transition ease-in-out delay-150 space-y-1.0 mt-2.5 mx-80 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800">
                                                            Add Mata Kuliah
                                                        </button>
                                                    </>
                                                }
                                            })()}
                                        </>
                                    }
                                })()}
                            </div>
                        </div>
                    </div>
                } else {
                    return <div className="divLoader">
                        <svg className="svgLoader" viewBox="0 0 100 100" width="10em" height="10em">
                            <path ng-attr-d="{{config.pathCmd}}" ng-attr-fill="{{config.color}}" stroke="none" d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#51CACC" transform="rotate(179.719 50 51)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 51;360 50 51" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></path>
                        </svg>
                    </div>
                }
            } else {
                return <div role="alert">
                    <div className="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                        ERR_CONNECTION_REFUSED
                    </div>
                </div>
            }
        })()}
    </>
    )
}

export default ListMataKuliahComponent