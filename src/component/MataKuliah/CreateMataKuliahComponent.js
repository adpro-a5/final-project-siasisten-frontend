import React, { useState } from 'react'
import MataKuliahService from '../../services/MataKuliah/MataKuliahService'
import { useNavigate } from 'react-router-dom';

const CreateMataKuliahComponent = () => {
    const history = useNavigate();
    const [idMataKuliah, setIdMataKuliah] = useState('')
    const [namaMataKuliah, setNamaMataKuliah] = useState('')
    const [jumlahLowongan, setJumlahLowongan] = useState('')

    const [dosen, setDosen] = useState([])

    const onChangeDosen = (nama, idx) => {
        const updateDosen = [...dosen];
        updateDosen[idx] = nama;
        setDosen(updateDosen);
    }

    const save = (e) => {
        e.preventDefault();
        let mataKuliah = {
            idMataKuliah: idMataKuliah,
            namaMataKuliah: namaMataKuliah,
            jumlahLowongan: jumlahLowongan,
            dosen: dosen
        }
        MataKuliahService.createMataKuliah(mataKuliah);
        history("/mata-kuliah")
    }

    const inputStyle = "appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"

    return (
        <div className="flex max-w-screen-lg w-full mx-auto gap-6 items-start font-['Montserrat']">
            <div className="flex-1 p-8 bg-white rounded-lg shadow">
                <h1 className="font-bold text-3xl mb-9 underline decoration-[#40606C] text-center">Formulir Registrasi Mata
                    Kuliah</h1>
                <form
                    className="mt-4 flex flex-col flex-1 gap-2 form-matkul"
                    method="POST"
                    autoComplete="off">
                    <label className="text-gray-700 text-sm font-bold" htmlFor="idMataKuliah"> ID Mata Kuliah </label>
                    <input
                        type="text"
                        name="idMataKuliah"
                        value={idMataKuliah}
                        placeholder="Masukkan ID Mata Kuliah"
                        className={inputStyle}
                        onChange={(e) => setIdMataKuliah(e.target.value)}
                    />
                    <label className="text-gray-700 text-sm font-bold" htmlFor="namaMataKuliah"> Nama Mata Kuliah </label>
                    <input
                        type="text"
                        name="namaMataKuliah"
                        value={namaMataKuliah}
                        placeholder="Masukkan Nama Mata Kuliah"
                        className={inputStyle}
                        onChange={(e) => setNamaMataKuliah(e.target.value)}
                    />
                    <label className="text-gray-700 text-sm font-bold" htmlFor="jumlahLowongan"> Jumlah Lowongan </label>
                    <input
                        type="text"
                        name="jumlahLowongan"
                        value={jumlahLowongan}
                        placeholder="Masukkan Jumlah Lowongan"
                        className={inputStyle}
                        onChange={(e) => setJumlahLowongan(e.target.value)}
                    />
                    <div>
                        <label className="text-gray-700 text-sm font-bold" htmlFor="jumlahDosen"> Jumlah Dosen </label>
                        <input
                            id="jumlah-dosen"
                            type="number"
                            name="jumlahDosen"
                            placeholder="Masukkan Jumlah Dosen Pengampu"
                            className={inputStyle}
                            value={dosen.length}
                            onChange={(e) => setDosen(new Array(parseInt(e.target.value)).fill(''))}
                        />
                    </div>
                    {dosen.map((nidn, i) => (
                        <div key={i}>
                            <label className="text-gray-700 text-sm font-bold" htmlFor={`nidn${i}`}> NIDN Dosen ke-{i + 1} </label>
                            <input
                                type="text"
                                name={`nidn${i}`}
                                placeholder={`Masukkan Dosen Ke-${i + 1}`}
                                className={inputStyle}
                                onChange={(e) => onChangeDosen(e.target.value, i)}
                            />
                        </div>
                    ))}
                    <button
                        className="transition ease-in-out delay-150 mx-72 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800"
                        type="submit"
                        onClick={(e) => save(e)}>
                        Daftar
                    </button>
                </form>
            </div>
        </div>
    )
}

export default CreateMataKuliahComponent