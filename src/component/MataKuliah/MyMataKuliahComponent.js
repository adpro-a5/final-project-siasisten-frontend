import React, { useEffect, useState } from 'react'
import DosenMKService from '../../services/MataKuliah/DosenMKService'
import AsdosMatkulService from '../../services/MataKuliah/AsdosMatkulService'
import MataKuliahService from '../../services/MataKuliah/MataKuliahService'

const PenilaianComponent = () => {
    const [principal, setPrincipal] = useState({})
    const [mataKuliahs, setMataKuliahs] = useState([null])

    useEffect(() => {
        async function fetchMyAPI() {
            const userData = localStorage.getItem("user")
            let user = ''
            if (typeof userData === 'object') {
                user = userData
            } else if (typeof userData === 'string') {
                user = JSON.parse(userData)
            }

            try {
                setPrincipal(user.principal)
            } catch (e) {
                setPrincipal(null)
            }

            if (user !== null && user.principal !== null) {
                if (user.principal.role === 'DOSEN') {
                    await DosenMKService.getMataKuliah(user.principal.email).then((res) => {
                        setMataKuliahs(res.data)
                    })
                } else if (user.principal.role === 'ASISTENDOSEN' || user.principal.role === 'KOORDINATOR') {
                    await AsdosMatkulService.getMataKuliah(user.principal.email).then((res) => {
                        setMataKuliahs(res.data)
                    })
                } else if (user.principal.role === 'ADMIN') {
                    await MataKuliahService.getMataKuliah().then((res) => {
                        console.log(res.data)
                        setMataKuliahs(res.data)
                    })
                }
            }
        }

        fetchMyAPI();
    }, []);

    return (
        <>
            {(() => {
                if (principal !== null) {
                    if (principal.role === 'MAHASISWA') {
                        return <div role="alert">
                            <div className="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                                You cannot access this page
                            </div>
                        </div>
                    } else if (mataKuliahs.length === 1 && mataKuliahs[0] === null) {
                        return <div role="alert">
                            <div className="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                                {principal.role}
                            </div>
                        </div>
                    } else {
                        if (mataKuliahs.length !== 0) {
                            return <div className="p-10 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3 gap-5">
                                {mataKuliahs.map(
                                    mataKuliah =>
                                        <div className="rounded overflow-hidden shadow-lg">
                                            <div className="px-6 pt-4">
                                                {(() => {
                                                    if (principal.role === 'ADMIN') {
                                                        return <>
                                                            <p className="font-bold text-base">
                                                                Id: {mataKuliah.idMataKuliah}
                                                            </p>
                                                            <div className="font-bold text-xl mb-2">{mataKuliah.namaMataKuliah}</div>
                                                        </>

                                                    } else {
                                                        return <>
                                                            <p className="font-bold text-base">
                                                                Id: {mataKuliah.mataKuliah.idMataKuliah}
                                                            </p>
                                                            <div className="font-bold text-xl mb-2">{mataKuliah.mataKuliah.namaMataKuliah}</div>
                                                        </>
                                                    }
                                                })()}
                                            </div>
                                            {(() => {
                                                if (principal.role === 'DOSEN') {
                                                    return <div className="px-6 pb-2">
                                                        <div className="flex flex-1 justify-center">
                                                            <a className="px-6 py-2 mr-2 mt-4 text-white bg-sky-600 rounded-lg hover:bg-sky-900" href={'/my-mata-kuliah/daftar-asisten/' + mataKuliah.mataKuliah.idMataKuliah + '/' + mataKuliah.mataKuliah.namaMataKuliah}>
                                                                List Asdos
                                                            </a>
                                                            <a className="px-6 py-2 mr-2 mt-4 text-white bg-sky-600 rounded-lg hover:bg-sky-900" href={'/my-mata-kuliah/penilaian-mahasiswa/' + mataKuliah.mataKuliah.idMataKuliah}>
                                                                Penilaian
                                                            </a>
                                                        </div>
                                                    </div>
                                                } else if (principal.role === 'ASISTENDOSEN' || principal.role === 'KOORDINATOR') {
                                                    return <div className="px-6 pb-2">
                                                        <div className="flex flex-1 justify-center">
                                                            <a className="px-6 py-2 mr-2 mt-4 text-white bg-sky-600 rounded-lg hover:bg-sky-900" href={'/my-mata-kuliah/penilaian-mahasiswa/' + mataKuliah.mataKuliah.idMataKuliah}>
                                                                Penilaian
                                                            </a>
                                                            <a className="px-6 py-2 mt-4 text-white bg-sky-600 rounded-lg hover:bg-sky-900" href={'/my-mata-kuliah/penugasan-asisten/' + mataKuliah.mataKuliah.idMataKuliah}>
                                                                Penugasan
                                                            </a>
                                                        </div>
                                                    </div>
                                                } else if (principal.role === 'ADMIN') {
                                                    return <div className="px-6 pb-2">
                                                        <div className="flex flex-1 justify-center">
                                                            <a className="px-6 py-2 mr-2 mt-4 text-white bg-sky-600 rounded-lg hover:bg-sky-900" href={'/my-mata-kuliah/daftar-asisten/' + mataKuliah.idMataKuliah + '/' + mataKuliah.namaMataKuliah}>
                                                                List Asdos
                                                            </a>
                                                        </div>
                                                    </div>
                                                }
                                            })()}
                                        </div>
                                )}
                            </div>
                        } else {
                            return <div className="flex flex-col flex-1 gap-2 text-center text-red-600">
                                Tidak ada Mata Kuliah yang Anda pegang
                            </div>
                        }
                    }
                } else {
                    return <div role="alert">
                        <div className="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                            You cannot access this page
                        </div>
                    </div>
                }
            })()}
        </>
    )
}

export default PenilaianComponent