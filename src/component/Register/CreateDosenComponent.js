import React, { useState } from 'react'
import DosenService from '../../services/Pengguna/DosenService'
import { useNavigate } from 'react-router-dom';

const CreateMataKuliahComponent = () => {
    const history = useNavigate();
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [nomorTelepon, setNomorTelepon] = useState('')

    const [nidnDosen, setNidnDosen] = useState('')
    const [namaDosen, setNamaDosen] = useState('')

    const save = (e) => {
        e.preventDefault();
        let akun = {
            email: email,
            password: password,
            nomorTelepon: nomorTelepon
        }
        let dosen = {
            nidnDosen: nidnDosen,
            namaDosen: namaDosen,
        }
        let akunDosen = {
            akun: akun,
            dosen: dosen
        }

        try {
            DosenService.createDosen(akunDosen);
            history('/');
        } catch(e) {
            alert("Register Gagal. Silahkan Register Email dan NIDN lain")
        }
    }

    const inputStyle = "appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"

    return (
        <div className="flex max-w-screen-lg w-full mx-auto gap-6 items-start font-['Montserrat']">
            <div className="flex-1 p-8 bg-white rounded-lg shadow">
                <h1 className="font-bold text-3xl mb-9 underline decoration-[#40606C] text-center">Formulir Registrasi Mata
                    Kuliah</h1>
                <form
                    className="mt-4 flex flex-col flex-1 gap-2 form-matkul"
                    method="POST"
                    autoComplete="off">
                    <label className="text-gray-700 text-sm font-bold" htmlFor="email"> Email Dosen </label>
                    <input
                        type="text"
                        name="email"
                        value={email}
                        placeholder="Masukkan Email Dosen"
                        className={inputStyle}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                    <label className="text-gray-700 text-sm font-bold" htmlFor="password"> Password </label>
                    <input
                        type="text"
                        name="password"
                        value={password}
                        placeholder="Masukkan Password"
                        className={inputStyle}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                    <label className="text-gray-700 text-sm font-bold" htmlFor="nomorTelepon"> Nomor Telepon </label>
                    <input
                        type="text"
                        name="nomorTelepon"
                        value={nomorTelepon}
                        placeholder="Masukkan Nomor Telepon"
                        className={inputStyle}
                        onChange={(e) => setNomorTelepon(e.target.value)}
                    />
                    <label className="text-gray-700 text-sm font-bold" htmlFor="nidnDosen"> NIDN Dosen </label>
                    <input
                        type="text"
                        name="nidnDosen"
                        value={nidnDosen}
                        placeholder="Masukkan NIDN Anda"
                        className={inputStyle}
                        onChange={(e) => setNidnDosen(e.target.value)}
                    />
                    <label className="text-gray-700 text-sm font-bold" htmlFor="namaDosen"> Nama Lengkap Dosen </label>
                    <input
                        type="text"
                        name="namaDosen"
                        value={namaDosen}
                        placeholder="Masukkan Nama Lengkap Anda"
                        className={inputStyle}
                        onChange={(e) => setNamaDosen(e.target.value)}
                    />
                    <button
                        className="transition ease-in-out delay-150 mx-72 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800"
                        type="submit"
                        onClick={(e) => save(e)}>
                        Daftar
                    </button>
                </form>
                <div className="flex justify-center mt-4">
                    <p>Sudah Punya Akun? <a href="/" className="text-sm text-blue-600 hover:underline"> Login</a></p>
                </div>
            </div>
        </div>
    )
}

export default CreateMataKuliahComponent