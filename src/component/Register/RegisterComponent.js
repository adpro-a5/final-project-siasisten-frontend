import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import Select from 'react-select';

const options = [
    { value: '/register-mahasiswa', label: 'Mahasiswa' },
    { value: '/register-dosen', label: 'Dosen' },
];

const CreateMataKuliahComponent = () => {
    const history = useNavigate();
    const [selectedOption, setSelectedOption] = useState(null);

    const save = (e) => {
        e.preventDefault();
        history(selectedOption.value)
    } 

    return (
        <div className="flex items-center justify-center min-h-screen bg-gray-100">
            <div className="px-12 py-12 mt-4 text-left bg-white shadow-lg">
                <div className="flex justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" className="w-20 h-20 text-blue-600" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path d="M12 14l9-5-9-5-9 5 9 5z" />
                        <path
                            d="M12 14l6.16-3.422a12.083 12.0d83 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14z" />
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                            d="M12 14l9-5-9-5-9 5 9 5zm0 0l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14zm-4 6v-7.5l4-2.222" />
                    </svg>
                </div>
                <h3 className="text-2xl font-bold text-center">Register your account</h3>
                <div className="flex mt-4 justify-center">
                    <Select
                        defaultValue={selectedOption}
                        onChange={setSelectedOption}
                        options={options}
                    />
                </div>
                <div className="flex flex-1 justify-center">
                    <button onClick={(e) => save(e)} className="px-6 py-2 mt-4 text-white bg-blue-600 rounded-lg hover:bg-blue-900">Register</button>
                </div>
            </div>  
        </div>

    )
}

export default CreateMataKuliahComponent