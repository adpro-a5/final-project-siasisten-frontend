import React, { useEffect } from 'react'
import LoginService from '../../services/Pengguna/LoginService'
import { useNavigate } from 'react-router-dom';

const LoginComponent = () => {
    const history = useNavigate();

    function handleNavigate(e) {
        e.preventDefault();
        history('/');
    }
    useEffect(() => {
        LoginService.logoutUser();
        localStorage.removeItem("user");
        localStorage.removeItem("asdos");
    }, []);

    return (
            <div className="bg-gray-100 w-full min-h-screen flex flex-col justify-center items-center py-16">
                Anda Berhasil Logout!
                    <button onClick={handleNavigate}
                        className="w-1/6 transition ease-in-out delay-50 space-y-1.0 mt-2.5 mx-80 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800">
                        Login
                    </button>
            </div>
    )
}

export default LoginComponent