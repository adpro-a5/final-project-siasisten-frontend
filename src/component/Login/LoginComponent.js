import React, { useState } from 'react'
import LoginService from '../../services/Pengguna/LoginService'
import AsdosMatkulService from '../../services/MataKuliah/AsdosMatkulService';
import { useNavigate } from 'react-router-dom';

const LoginComponent = () => {
    const [login, setLogin] = useState({
        data: {
          authenticated: false
        }
      })
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const history = useNavigate();

    const inputStyle = "appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"

    const onLogin = (e) => {
        e.preventDefault();
        let akun = {
            email: email,
            password: password
        }
        LoginService.loginUSer(akun).then((res) => {
            let login = {
                data: res.data
            }
            setLogin(login)
            localStorage.setItem("user", JSON.stringify(res.data))
        });
    }

    if (login.data.authenticated && login.data.principal !== "anonymousUser") {
        if (login.data.principal.role === "ASISTENDOSEN" || login.data.principal.role === "KOORDINATOR") {
            AsdosMatkulService.getMataKuliah(login.data.principal.email).then((res) => {
                console.log(res.data)
                localStorage.setItem("asdos", JSON.stringify(res.data))
            })
        }
        history('/mata-kuliah');
    }

    return (
        <div className="bg-gray-100 w-full min-h-screen flex flex-col justify-center items-center py-16">
            <div className="flex max-w-screen-lg w-full mx-auto gap-6 items-start">
                <div className="flex-1 p-8 bg-white rounded-lg shadow">
                    <h3 className="text-xl font-bold">Login</h3>
                    <form
                        className="mt-4 flex flex-col flex-1 gap-2"
                        method="POST"
                        autoComplete="off">
                        <label className="text-gray-700 text-sm font-bold" htmlFor="username"> Email </label>
                        <input
                            type="text"
                            name="username"
                            value={email}
                            placeholder="Masukkan Email Anda"
                            className={inputStyle}
                            onChange={(e) => setEmail(e.target.value)}
                        />
                        <label className="text-gray-700 text-sm font-bold" htmlFor="password"> Password </label>
                        <input
                            type="password"
                            name="password"
                            value={password}
                            placeholder="Masukkan Password"
                            className={inputStyle}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                        <button
                            className="bg-blue-500 text-white hover:bg-blue-600 rounded px-6 py-2 mt-2"
                            type="submit"
                            onClick={(e) => onLogin(e)}
                        >
                            Login
                        </button>
                    </form>
                </div>
            </div>
            <div className="flex justify-center mt-4">
                <p>Tidak Punya Akun? <a href="/register" className="text-sm text-blue-600 hover:underline"> Register here</a></p>
            </div>
        </div>
    )
}

export default LoginComponent