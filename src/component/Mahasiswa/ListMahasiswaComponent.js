import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import MahasiswaService from '../../services/Pengguna/MahasiswaService'

const ListMahasiswaComponent = () => {
    const [principal, setPrincipal] = useState({})
    const [mahasiswas, setMahasiswas] = useState([])
    const [err, serErr] = useState(null)
    const history = useNavigate();

    function handleNavigate(e) {
        e.preventDefault();
        history('/mata-kuliah');
    }

    useEffect(() => {
        MahasiswaService.getMahasiswa().then((res) => {
            setMahasiswas(res.data)
        }).catch((error) => {
            serErr(error)
        });
    }, []);

    useEffect(() => {
        const userData = localStorage.getItem("user")
        let user = ''
        if (typeof userData === 'object') {
            user = userData
        } else if (typeof userData === 'string') {
            user = JSON.parse(userData)
        }

        try {
            setPrincipal(user.principal)
        } catch (e) {
            setPrincipal(null)
        }
    }, []);

    return (
        <>
            {(() => {
                if (err === null) {
                    if (principal !== null && principal.role === "ADMIN") {
                        if (mahasiswas.length !== 0) {
                            return <div className="flex max-w-screen-lg w-full mx-auto gap-6 items-start font-['Montserrat']">
                                <div className="flex-1 p-8 bg-white rounded-lg shadow">
                                    <h1 className="font-bold text-3xl mb-9 underline decoration-[#780000] bg-[#EDEDED] text-[#0A0A0A] text-center">Daftar Mahasiswa</h1>
                                    <div className="flex flex-col gap-4">
                                        <table className="table-auto">
                                            <thead className="text-l uppercase bg-[#EDEDED] text-[#0A0A0A]">
                                                <tr>
                                                    <th>Nama</th>
                                                    <th>NPM</th>
                                                    <th>E-mail</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                {
                                                    mahasiswas.map(
                                                        mahasiswa =>
                                                            <tr className="bg-white border-b dark:bg-gray-50 dark:border-gray-20 
                                                hover:bg-gray-50 dark:hover:bg-[#E0E0E0] dark:text-[#424242] whitespace-nowrap">
                                                                <td>
                                                                    <div className="text-center">
                                                                        {mahasiswa.namaMahasiswa}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="text-center">
                                                                        {mahasiswa.npmMahasiswa}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="text-center">
                                                                        {mahasiswa.akun.email}
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                    )
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div >
                        } else {
                            return <div className="divLoader">
                                <svg className="svgLoader" viewBox="0 0 100 100" width="10em" height="10em">
                                    <path ng-attr-d="{{config.pathCmd}}" ng-attr-fill="{{config.color}}" stroke="none" d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#51CACC" transform="rotate(179.719 50 51)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 51;360 50 51" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></path>
                                </svg>
                            </div>
                        }
                    } else {
                        return <>
                            <div role="alert">
                                <div className="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                                    Can't be Accessed!
                                </div>
                                <div className="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                                    <p>You must have role ADMIN to access this page.</p>
                                </div>
                            </div>
                            <div className="flex flex-col justify-center items-center">
                                <button onClick={handleNavigate}
                                    className="transition ease-in-out delay-50 space-y-1.0 mt-2.5 mx-80 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800">
                                    Back to Home
                                </button>
                            </div>
                        </>
                    }
                } else {
                    return <div role="alert">
                        <div className="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                            ERR_CONNECTION_REFUSED
                        </div>
                    </div>
                }
            })()}
        </>
    )
}

export default ListMahasiswaComponent