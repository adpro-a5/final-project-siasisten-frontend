import React, { useEffect, useState } from 'react'
import MahasiswaService from '../../services/Pengguna/MahasiswaService'
import { useNavigate } from 'react-router-dom';

const ProfileComponent = () => {
    const history = useNavigate();
    const [principal, setPrincipal] = useState({})
    const [mahasiswa, setMahasiswa] = useState('')
    const [waktuKosong, setWaktuKosong] = useState('')
    const [namaBank, setNamaBank] = useState('')
    const [nomorRekening, setNomorRekening] = useState('')
    const [akun, setAkun] = useState('')
    const [nomorTelepon, setNomorTelepon] = useState('')

    function handleNavigate(e) {
        e.preventDefault();
        history('/mata-kuliah');
    }

    useEffect(() => {
        async function fetchMyAPI() {
            const userData = localStorage.getItem("user")
            let user = ''
            if (typeof userData === 'object') {
                user = userData
            } else if (typeof userData === 'string') {
                user = JSON.parse(userData)
            }

            try {
                setPrincipal(user.principal)
            } catch (e) {
                setPrincipal(null)
            }

            if (user !== null) {
                MahasiswaService.getMahasiswaByEmail(user.name).then((res) => {
                    setMahasiswa(res.data)
                    setWaktuKosong(res.data.waktuKosong)
                    setNamaBank(res.data.namaBank)
                    setNomorRekening(res.data.nomorRekening)
                    setAkun(res.data.akun)
                    setNomorTelepon(res.data.akun.nomorTelepon)
                })
            }
        }

        fetchMyAPI();
    }, []);

    const update = (e) => {
        e.preventDefault();
        let update = {
            nomorTelepon: nomorTelepon,
            waktuKosong: waktuKosong,
            namaBank: namaBank,
            nomorRekening: nomorRekening
        }
        MahasiswaService.updateMahasiswa(mahasiswa.npmMahasiswa, update);
        history("/mata-kuliah")
    }

    const inputStyle = "appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"

    return (
        <>
            {(() => {
                if (principal !== null &&
                    (principal.role === "MAHASISWA" || principal.role === "ASISTENDOSEN" || principal.role === "KOORDINATOR")) {
                    if (mahasiswa !== '') {
                        return <div className="flex max-w-screen-lg w-full mx-auto gap-6 items-start font-['Montserrat']">
                            <div className="flex-1 p-8 bg-white rounded-lg shadow">
                                <div class="flex justify-center mb-2">
                                    <img className="object-center rounded w-36 h-32" src="https://www.kindpng.com/picc/m/252-2524695_dummy-profile-image-jpg-hd-png-download.png" alt="Extra large avatar"></img>
                                </div>
                                <div class="flex justify-center">
                                    <button class="bg-gray-500 text-white active:bg-pink-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" type="button">
                                        Edit Image
                                    </button>
                                </div>
                                <form
                                    className="mt-4 flex flex-col flex-1 gap-2 form-matkul"
                                    method="POST"
                                    autoComplete="off">
                                    <label className="text-gray-700 text-sm font-bold" htmlFor="email"> Email Mahasiswa </label>
                                    <input readOnly
                                        type="text"
                                        name="email"
                                        value={akun.email}
                                        placeholder="Masukkan Email Mahasiswa"
                                        className={inputStyle}
                                    />
                                    <label className="text-gray-700 text-sm font-bold" htmlFor="nidnMahasiswa"> NPM Mahasiswa </label>
                                    <input readOnly
                                        type="text"
                                        name="nidnMahasiswa"
                                        value={mahasiswa.npmMahasiswa}
                                        placeholder="Masukkan NIDN Anda"
                                        className={inputStyle}
                                    />
                                    <label className="text-gray-700 text-sm font-bold" htmlFor="namaMahasiswa"> Nama Mahasiswa </label>
                                    <input readOnly
                                        type="text"
                                        name="namaMahasiswa"
                                        value={mahasiswa.namaMahasiswa}
                                        placeholder="Masukkan Nama Lengkap Anda"
                                        className={inputStyle}
                                    />
                                    <label className="text-gray-700 text-sm font-bold" htmlFor="nomorTelepon"> Nomor Telepon </label>
                                    <input
                                        type="text"
                                        name="nomorTelepon"
                                        value={nomorTelepon || ''}
                                        placeholder="Masukkan Nomor Telepon"
                                        className={inputStyle}
                                        onChange={(e) => setNomorTelepon(e.target.value)}
                                    />
                                    <label className="text-gray-700 text-sm font-bold" htmlFor="waktuKosong"> Waktu Kosong </label>
                                    <textarea
                                        type="text"
                                        name="namaMahasiswa"
                                        value={waktuKosong || ''}
                                        placeholder="Masukkan Waktu Kosong Anda"
                                        className={inputStyle}
                                        onChange={(e) => setWaktuKosong(e.target.value)}
                                    >
                                    </textarea>
                                    <label className="text-gray-700 text-sm font-bold" htmlFor="namaBank"> Nama Bank </label>
                                    <input
                                        type="text"
                                        name="namaBank"
                                        value={namaBank || ''}
                                        placeholder="Masukkan Nama Bank Yang Anda Gunakan"
                                        className={inputStyle}
                                        onChange={(e) => setNamaBank(e.target.value)}
                                    />
                                    <label className="text-gray-700 text-sm font-bold" htmlFor="nomorRekening"> Nomor Rekening </label>
                                    <input
                                        type="text"
                                        name="nomorRekening"
                                        value={nomorRekening || ''}
                                        placeholder="Masukkan Nomor Rekening Anda"
                                        className={inputStyle}
                                        onChange={(e) => setNomorRekening(e.target.value)}
                                    />
                                    <button
                                        className="transition ease-in-out delay-150 mx-72 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800"
                                        type="submit"
                                        onClick={(e) => update(e)}>
                                        Update
                                    </button>
                                </form>
                            </div>
                        </div>
                    } else {
                        return <div className="divLoader">
                            <svg className="svgLoader" viewBox="0 0 100 100" width="10em" height="10em">
                                <path ng-attr-d="{{config.pathCmd}}" ng-attr-fill="{{config.color}}" stroke="none" d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#51CACC" transform="rotate(179.719 50 51)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 51;360 50 51" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></path>
                            </svg>
                        </div>
                    }
                } else {
                    return <>
                        <div role="alert">
                            <div className="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                                Can't be Accessed!
                            </div>
                            <div className="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                                <p>You must have role MAHASISWA to access this page.</p>
                            </div>
                        </div>
                        <div className="flex flex-col justify-center items-center">
                            <button onClick={handleNavigate}
                                className="transition ease-in-out delay-50 space-y-1.0 mt-2.5 mx-80 text-blue-700 hover:-translate-y-1 hover:scale-110 hover:text-white border border-blue-700 hover:bg-blue-800 duration-300 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm py-2.5 text-center dark:border-blue-500 dark:text-blue-500 dark:hover:text-white dark:hover:bg-blue-600 dark:focus:ring-blue-800">
                                Back to Home
                            </button>
                        </div>
                    </>
                }
            })()}
        </>
    )
}

export default ProfileComponent