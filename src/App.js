import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import ListMataKuliahComponent from './component/MataKuliah/ListMataKuliahComponent'
import ListMahasiswaComponent from './component/Mahasiswa/ListMahasiswaComponent'
import ListDosenComponent from './component/Dosen/ListDosenComponent'
import ListPendaftaranAsdosComponent from './component/Pendaftaran/ListPendaftaranAsdosComponent'
import CreateAsistenDosenComponent from './component/Pendaftaran/CreateAsistenDosenComponent'
import CreateDosenComponent from './component/Register/CreateDosenComponent'
import CreateMahasiswaComponent from './component/Register/CreateMahasiswaComponent'
import CreateMataKuliahComponent from './component/MataKuliah/CreateMataKuliahComponent';
import RegisterComponent from './component/Register/RegisterComponent'
import LoginComponent from './component/Login/LoginComponent';
import LogoutComponent from './component/Login/LogoutComponent';
import FormKriteriaPenilaianComponent from './component/Penilaian/FormKriteriaPenilaianComponent';
import FormMahasiswaMatkulComponent from './component/Penilaian/FormMahasiswaMatkulComponent';
import PenilaianMahasiswaComponent from './component/Penilaian/PenilaianMahasiswaComponent';
import PenilaianMahasiswaEditComponent from './component/Penilaian/PenilaianMahasiswaEditComponent';
import MyMataKuliahComponent from './component/MataKuliah/MyMataKuliahComponent';
import WithNav from "./component/Navbar/WithNav"
import WithoutNav from "./component/Navbar/WithoutNav"
import ProfileComponent from './component/Mahasiswa/ProfileComponent';
import ListAsistenDosenComponent from './component/Pendaftaran/ListAsistenDosenComponent'
import LogAsistenComponent from './component/LogAsisten/LogAsistenComponent';
import CreateLogAsistenComponent from './component/LogAsisten/CreateLogAsistenComponent';
import UpdateLogAsistenComponent from './component/LogAsisten/UpdateLogAsistenComponent';
import LaporanLogAsistenComponent from './component/LogAsisten/LaporanLogAsistenComponent';
import PenugasanComponent from './component/Penugasan/PenugasanComponent';
import DetailMahasiswaComponent from './component/Mahasiswa/DetailMahasiswaComponent';
import LogPerAsistenDosenComponent from './component/LogAsisten/LogPerAsistenDosenComponent';

function App() {
  return (
    <Router>
      <div className="container">
          <Routes>
            <Route element={<WithoutNav />}>
              <Route path="" element={<LoginComponent />}></Route>
              <Route path="/logout" element={<LogoutComponent />}></Route>
              <Route path="/register" element={<RegisterComponent />}></Route>
              <Route path="/register-mahasiswa" element={<CreateMahasiswaComponent />}></Route>
              <Route path="/register-dosen" element={<CreateDosenComponent />}></Route>
            </Route>
            <Route element={<WithNav />}>
              <Route path="/mata-kuliah" element={<ListMataKuliahComponent />}></Route>
              <Route path="/add-mata-kuliah" element={<CreateMataKuliahComponent />}></Route>
              <Route path="/mahasiswa" element={<ListMahasiswaComponent />}></Route>
              <Route path="/dosen" element={<ListDosenComponent />}></Route>
              <Route path="/asisten-dosen/:id/:nama" element={<ListPendaftaranAsdosComponent />}></Route>
              <Route path="/asisten-dosen/detail-pendaftar/:id/:email" element={<DetailMahasiswaComponent />}></Route>
              <Route path="/pendaftaran/:id/:nama" element={<CreateAsistenDosenComponent />}></Route>
              <Route path="/my-mata-kuliah" element={<MyMataKuliahComponent />}></Route>
              <Route path="/my-mata-kuliah/daftar-asisten/:id/:nama" element={<ListAsistenDosenComponent />}></Route>
              <Route path="/my-mata-kuliah/daftar-asisten/detail-asisten/:id/:email" element={<DetailMahasiswaComponent />}></Route>
              <Route path="/my-mata-kuliah/daftar-asisten/log-asisten/:id/:npm" element={<LogPerAsistenDosenComponent />}></Route>
              <Route forceRefresh={true} path="/my-mata-kuliah/penilaian-mahasiswa/:id" element={<PenilaianMahasiswaComponent />}></Route>
              <Route path="/my-mata-kuliah/penilaian-mahasiswa/edit/:id" element={<PenilaianMahasiswaEditComponent />}></Route>
              <Route path="/my-mata-kuliah/penilaian-mahasiswa/add-kriteria/:id" element={<FormKriteriaPenilaianComponent />}></Route>
              <Route path="/my-mata-kuliah/penilaian-mahasiswa/add-mahasiswa/:id" element={<FormMahasiswaMatkulComponent />}></Route>
              <Route path="/my-mata-kuliah/penugasan-asisten/:id" element={<PenugasanComponent />}></Route>
              <Route path="/profile-mahasiswa" element={<ProfileComponent />}></Route>
              <Route forceRefresh={true} path="/log-asisten" element={<LogAsistenComponent />}></Route>
              <Route path="/log-asisten/add-log" element={<CreateLogAsistenComponent />}></Route>
              <Route path="/log-asisten/update-log/:id" element={<UpdateLogAsistenComponent />}></Route>
              <Route path="/laporan-log-asisten" element={<LaporanLogAsistenComponent />}></Route>
            </Route>
          </Routes>
      </div>
    </Router>
  );
}

export default App;
