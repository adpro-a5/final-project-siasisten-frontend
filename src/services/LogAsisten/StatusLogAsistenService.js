import axios from 'axios';

const SLA_MK_API_BASE_URL = "https://siasisten-plus-v3.herokuapp.com/api/status-log-asisten";

class StatusLogAsistenService {
    getAllLogAsistenByIdMataKuliah(id, request) {
        return axios.get(SLA_MK_API_BASE_URL + "/" + id, { params:request });
    }

    updateLogAsisten(id, request) {
        return axios.put(SLA_MK_API_BASE_URL + "/" + id, request);
    }

}

export default new StatusLogAsistenService()