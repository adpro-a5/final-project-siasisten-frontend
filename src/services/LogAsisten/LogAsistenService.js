import axios from 'axios';

const LA_MK_API_BASE_URL = "https://siasisten-plus-v3.herokuapp.com/api/log-asisten";

class LogAsistenService {
    getLogAsistenByIdLog(id) {
        return axios.get(LA_MK_API_BASE_URL + "/" + id);
    }

    createLogAsisten(log) {
        return axios.post(LA_MK_API_BASE_URL, log);
    }

    updateLogAsisten(id, log) {
        return axios.put(LA_MK_API_BASE_URL + "/" + id, log);
    }

    deleteLogAsisten(id) {
        return axios.delete(LA_MK_API_BASE_URL + "/" + id);
    }
}

export default new LogAsistenService()