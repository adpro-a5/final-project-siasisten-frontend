import axios from 'axios';

const LLA_MK_API_BASE_URL = "https://siasisten-plus-v3.herokuapp.com/api/laporan-log-asisten";

class LaporanLogAsistenService {
    getAllLaporan(data) {
        return axios.get(LLA_MK_API_BASE_URL, { params: data });
    }

    getAllLaporanByStatus(status, data) {
        return axios.get(LLA_MK_API_BASE_URL + '/' + status, { params: data });
    }
}

export default new LaporanLogAsistenService()