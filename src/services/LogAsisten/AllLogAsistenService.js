import axios from 'axios';

const LA_MK_API_BASE_URL = "https://siasisten-plus-v3.herokuapp.com/api/all-log-asisten";

class AllLogAsistenService {
    getAllLogAsisten(npm) {
        return axios.get(LA_MK_API_BASE_URL, { params: npm });
    }

    getAllLogAsistenByIdMataKuliah(npm, id) {
        return axios.get(LA_MK_API_BASE_URL + "/" + id, { params: npm });
    }
}

export default new AllLogAsistenService()