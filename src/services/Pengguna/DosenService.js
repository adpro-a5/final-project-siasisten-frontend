import axios from 'axios';

const DOSEN_API_BASE_URL = "https://siasisten-plus.herokuapp.com/api/dosen";

class DosenService {
    getDosen(){
        return axios.get(DOSEN_API_BASE_URL);
    }

    createDosen(akunDosen){
        return axios.post(DOSEN_API_BASE_URL, akunDosen);
    }

    getDosenByEmail(email){
        return axios.get(DOSEN_API_BASE_URL + '/' + email);
    }

    updateDosen(nidn, action){
        return axios.put(DOSEN_API_BASE_URL + '/' + nidn, action);
    }
}

export default new DosenService()
