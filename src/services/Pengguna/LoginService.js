import axios from 'axios';

const LOGIN_API_BASE_URL = "https://siasisten-plus.herokuapp.com/auth/login";
const LOGOUT_API_BASE_URL = "https://siasisten-plus.herokuapp.com/auth/logout";

class LoginService {
    loginUSer(akun){
        return axios.post(LOGIN_API_BASE_URL, akun);
    }

    logoutUser(){
        return axios.post(LOGOUT_API_BASE_URL);
    }
}

export default new LoginService()
