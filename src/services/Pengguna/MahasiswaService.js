import axios from 'axios';

const MAHASISWA_API_BASE_URL = "https://siasisten-plus.herokuapp.com/api/mahasiswa";

class MahasiswaService {
    getMahasiswa(){
        return axios.get(MAHASISWA_API_BASE_URL);
    }

    getMahasiswaByEmail(email){
        return axios.get(MAHASISWA_API_BASE_URL + '/' + email);
    }

    createMahasiswa(mahasiswa){
        return axios.post(MAHASISWA_API_BASE_URL, mahasiswa);
    }

    updateMahasiswa(npm, update){
        return axios.put(MAHASISWA_API_BASE_URL + '/' + npm, update);
    }


}

export default new MahasiswaService()
