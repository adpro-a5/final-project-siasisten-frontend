import axios from 'axios';

const PENILAIAN_API_BASE_URL = "https://siasisten-plus-v2.herokuapp.com/api/penilaian-mahasiswa";

class PenilaianMahasiswaService {
    getPenilaianByIdMataKuliah(id){
        return axios.get(PENILAIAN_API_BASE_URL + '/' + id);
    }

    updateNilai(penilaian) {
        return axios.post(PENILAIAN_API_BASE_URL, penilaian)
    }
}

export default new PenilaianMahasiswaService()