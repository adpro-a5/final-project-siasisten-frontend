import axios from 'axios';

const MM_API_BASE_URL = "https://siasisten-plus-v2.herokuapp.com/api/mahasiswa-matkul";

class MahasiswaMatkulService {
    getMahasiswaByIdMataKuliah(id){
        return axios.get(MM_API_BASE_URL + '/' + id);
    }
    
    createMahasiswaMatkul(mahasiswaMatkul){
        return axios.post(MM_API_BASE_URL, mahasiswaMatkul);
    }
}

export default new MahasiswaMatkulService()