import axios from 'axios';

const KRITERIA_API_BASE_URL = "https://siasisten-plus-v2.herokuapp.com/api/kriteria-penilaian";

class KriteriaPenilaianService {
    getKriteriaByIdMataKuliah(id){
        return axios.get(KRITERIA_API_BASE_URL + '/' + id);
    }

    createKriteria(kriteria){
        return axios.post(KRITERIA_API_BASE_URL, kriteria);
    }
}

export default new KriteriaPenilaianService()