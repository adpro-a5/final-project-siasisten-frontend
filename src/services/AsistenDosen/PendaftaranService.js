import axios from 'axios';

const PENDAFTARAN_API_BASE_URL = "https://siasisten-plus.herokuapp.com/api/pendaftaran";

class PendaftaranService {
    getPendaftaranByIdMataKuliah(idMataKuliah) {
        return axios.get(PENDAFTARAN_API_BASE_URL + '/' + idMataKuliah);
    }

    createPendaftaran(pendaftaran) {
        return axios.post(PENDAFTARAN_API_BASE_URL, pendaftaran);
    }

    actionPendaftaran(id, action) {
        return axios.put(PENDAFTARAN_API_BASE_URL + '/' + id, action);
    }
}

export default new PendaftaranService()