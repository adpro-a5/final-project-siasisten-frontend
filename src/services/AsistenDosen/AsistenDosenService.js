import axios from 'axios';

const ASDOS_API_BASE_URL = "https://siasisten-plus.herokuapp.com/api/asisten-dosen";

class AsistenDosenService {
    getAsistenDosenByIdMataKuliah(idMataKuliah) {
        return axios.get(ASDOS_API_BASE_URL + '/' + idMataKuliah);
    }
}

export default new AsistenDosenService()