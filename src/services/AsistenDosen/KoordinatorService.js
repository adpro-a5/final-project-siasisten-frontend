import axios from 'axios';

const KOORDINATOR_API_BASE_URL = "https://siasisten-plus.herokuapp.com/api/koordinator";

class KoordinatorService {
    assignKoordinator(assign) {
        return axios.post(KOORDINATOR_API_BASE_URL, assign);
    }
}

export default new KoordinatorService()