import axios from 'axios';

const MATA_KULIAH_API_BASE_URL = "https://siasisten-plus.herokuapp.com/api/mata-kuliah";

class MataKuliahService {
    getMataKuliah() {
        return axios.get(MATA_KULIAH_API_BASE_URL);
    }

    createMataKuliah(mataKuliah) {
        return axios.post(MATA_KULIAH_API_BASE_URL, mataKuliah);
    }
}

export default new MataKuliahService()