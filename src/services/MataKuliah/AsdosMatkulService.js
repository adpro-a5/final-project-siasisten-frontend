import axios from 'axios';

const ASDOS_MK_API_BASE_URL = "https://siasisten-plus.herokuapp.com/api/asdos-matkul";

class AsdosMatkulService {
    getMataKuliah(email) {
        return axios.get(ASDOS_MK_API_BASE_URL + '/' + email);
    }
}

export default new AsdosMatkulService()