import axios from 'axios';

const DOSEN_MK_API_BASE_URL = "https://siasisten-plus.herokuapp.com/api/dosen-matkul";

class DosenMKService {
    getMataKuliah(email) {
        return axios.get(DOSEN_MK_API_BASE_URL + '/' + email);
    }
}

export default new DosenMKService()