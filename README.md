# Final Project siasisten+

### Repository:
Repository Frontend: https://gitlab.com/adpro-a5/final-project-siasisten-frontend

Repository Backend - Pertama: https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006595753-Ramdhan-Firdaus-Amelia/adpro-a5/final-project-siasisten-plus-pengguna

Repository Backend - Kedua: https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006595753-Ramdhan-Firdaus-Amelia/adpro-a5/final-project-siasisten-plus-penilaian-penugasan

Repository Backend - Ketiga: https://gitlab.cs.ui.ac.id/AdvProg/reguler-2022/student/kelas-a/2006595753-Ramdhan-Firdaus-Amelia/adpro-a5/final-project-siasisten-plus-log-asisten

### Link Penting:
Link Heroku Frontend: https://siasisten-plus.netlify.app/

Link Heroku Backend-1: https://siasisten-plus.herokuapp.com/

Link Heroku Backend-2: https://siasisten-plus-v2.herokuapp.com/

Link Heroku Backend-3: -

Link Sheet PBI: https://ristek.link/PBI-Siasisten+

Link Data Dummy: https://ristek.link/Data-Dummy-Siasisten+

## Informasi Sprint 1
### Informasi PBI
| Nama PBI | Yang Mengerjakan           | Styling  |
| ------------------- | ------------- | ----- |
| Registrasi Pengguna | Ramdhan Firdaus Amelia, Muhammad Ichsan Khairullah | M Margaretha Stella Kalyanaduhita Tisera, Ramdhan Firdaus Amelia |
| Login & Logout | Muhammad Ichsan Khairullah, Ramdhan Firdaus Amelia | Muhammad Ichsan Khairullah, Ramdhan Firdaus Amelia |
| Mendaftarkan Mata Kuliah | Ramdhan Firdaus Amelia | M Margaretha Stella Kalyanaduhita Tisera |
| Daftar Lowongan | Ramdhan Firdaus Amelia, M Margaretha Stella Kalyanaduhita Tisera  | M Margaretha Stella Kalyanaduhita Tisera |
| Daftar Asisten Dosen | Atifah Nabilla Al Qibtiyah | M Margaretha Stella Kalyanaduhita Tisera |

### Informasi Testing
| Testing              | Yang Mengerjakan |
|----------------------| ------------ |
| Controller           | Ramdhan Firdaus Amelia |
| Core                 | Ramdhan Firdaus Amelia |
| Model                | Ramdhan Firdaus Amelia |
| AkunDetailServiceTest | Ramdhan Firdaus Amelia |
| DosenServiceImplTest | M Margaretha Stella Kalyanaduhita Tisera |
| MahasiswaServiceTest | Muhammad Ichsan Khairullah |
| MataKuliahServiceImplTest | M Margaretha Stella Kalyanaduhita Tisera |
| PendaftaranServiceTest | Atifah Nabilla Al Qibtiyah |

### Informasi Lainnya
| Task              | Yang Mengerjakan |
|----------------------| ------------ |
| Deployment           | Ramdhan Firdaus Amelia |
| Pembuatan Dummy                 | M Margaretha Stella Kalyanaduhita Tisera |

## Informasi Sprint 2
### Informasi PBI
| Nama PBI                                                | Yang Mengerjakan                                                | Styling  |
|---------------------------------------------------------|-----------------------------------------------------------------| ----- |
| Melihat Daftar dan Memberikan Rekomendasi Asisten Dosen | Atifah Nabilla Al Qibtiyah, Ramdhan Firdaus Amelia | Atifah Nabilla Al Qibtiyah |
| Pengelolaan Asisten Dosen | Atifah Nabilla Al Qibtiyah, Ramdhan Firdaus Amelia | Atifah Nabilla Al Qibtiyah |
| Assign Koordinator | M Margaretha Stella Kalyanaduhita Tisera, Ramdhan Firdaus Amelia | M Margaretha Stella Kalyanaduhita Tisera |
| Daftar Tugas Asisten Dosen [Belum Selesai] | Muhammad Ichsan Khairullah | Muhammad Ichsan Khairullah |
| Memasukkan kriteria penilaian dari Mata Kuliah | Ramdhan Firdaus Amelia | Ramdhan Firdaus Amelia |
| Memasukkan Nilai Mahasiswa | Ramdhan Firdaus Amelia | Ramdhan Firdaus Amelia |

### Informasi Testing
| Testing                       | Yang Mengerjakan |
|-------------------------------|------------------|
| PendaftaranControllerTest     | Atifah Nabilla Al Qibtiyah |
| PendaftaranApiControllerTest  | Ramdhan Firdaus Amelia |
| PendaftaranServiceTest        | Atifah Nabilla Al Qibtiyah, Ramdhan Firdaus Amelia |
| AsistenDosenApiControllerTest | Ramdhan Firdaus Amelia |
| AsistenDosenServiceTest       | Ramdhan Firdaus Amelia |
| KoordinatorControllerTest     | M Margaretha Stella Kalyanaduhita Tisera |
| KoordinatorApiControllerTest  | Ramdhan Firdaus Amelia |
| KoordinatorServiceTest        | M Margaretha Stella Kalyanaduhita Tisera |
| KriteriaPenilaianApiControllerTest | Ramdhan Firdaus Amelia |
| KriteriaPenilaianServiceTest       | Ramdhan Firdaus Amelia |
| MahasiswaMatkulApiControllerTest | Ramdhan Firdaus Amelia |
| MahasiswaMatkulServiceTest       | Ramdhan Firdaus Amelia |
| PenilaianMahasiswaApiControllerTest | Ramdhan Firdaus Amelia |
| PenilaianMahasiswaServiceTest       | Ramdhan Firdaus Amelia |

### Informasi Lainnya
| Task                                             | Yang Mengerjakan |
|--------------------------------------------------| ------------ |
| Deployment Backend 2 | Ramdhan Firdaus Amelia |
| Deployment Frontend  | Ramdhan Firdaus Amelia |
| Penyelesaian PBI untuk Sprint 3 - Update Profile | Ramdhan Firdaus Amelia |

| Kaitan dengan Materi       | Letak Code                                                                          |
|----------------------------|-------------------------------------------------------------------------------------|
| Design Pattern - Singleton | Authority                                                                           |
| Microservices              | 1 Frontend - 2 Backend                                                              |
| Asynchronous               | PBI Memasukkan Nilai Mahasiswa (KriteriaPenilaian, Mahasiswa, Penilaian)            |
| Concurrency                | fetchAllDosen - DosenService && getAsistenDosenByIdMataKuliah - AsistenDosenService |
